package org.drupal.module.pivots.pivots4do;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Sep 22, 2008
 */
public class DoubleCorrRecencyAlgorithm extends DoubleCorrAlgorithm {

    Connection conn;
    PreparedStatement stmt;
    int curr_ts;

    public void init() {
        try {
            super.PID = 4154;
            conn = getDrupalConnection();
            // use node_comments_statistics as the latest timestamp. if no comments, it's then the changed field of the node
            stmt = conn.prepareStatement("SELECT last_comment_timestamp ts FROM node_comment_statistics WHERE nid=?");
            curr_ts = currentUnixTime();
            //logger.info("Starting timestamp: "+curr_ts);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("database error");
        }
    }

    public void reindex() {
        super.reindex();
        try {
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println("DB connection close error. Ignore.");
        }
    }

    protected float getWeight(int src_id, int dst_id) {
        float weight = 0;
        try {
            stmt.setInt(1, dst_id);
            ResultSet rs = stmt.executeQuery();
            rs.first();
            int node_ts = rs.getInt(1);
            float dayDiff = (curr_ts - node_ts) /  86400f;
            weight = calculateWeigh(dayDiff);
        } catch (SQLException e) {
            System.err.println("Node not exists: "+dst_id);
        }
        return weight;
    }

    private float calculateWeigh(float dayDiff) {
        // half life decay
        //return Math.pow(0.5, dayDiff/60); // 0.5^(x/60), when x=600, y is about 0.001

        // linear decay
        float weight = 1-dayDiff/600; // when x=600, y=0
        weight = (weight>0) ? weight : 0;
        return weight;
    }
}
