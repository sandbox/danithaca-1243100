package org.drupal.module.pivots.pivots4do;

import java.sql.*;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import static magicstudio.util.MathOperation.*;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Aug 6, 2008
 */
public class DoubleCorrAlgorithm extends Algorithm{

    Map<Integer, Integer> projectMap = new TreeMap<Integer, Integer>();
    Map<Integer, Integer> forumMap = new TreeMap<Integer, Integer>();
    List<Integer> projectIndex = new ArrayList<Integer>();
    List<Integer> forumIndex = new ArrayList<Integer>();

    protected int timestamp;

    public void init() {
        super.PID = 4153;
    }

    public void index() {
        logger.info("Attention: the current implementation will do full reindex in an incremental index cycle");
        reindex();
    }

    protected String getRelationSql() {
        return "select src_id, dest_id from pivots_match where pivot_id=4151 and a2 in (10, 15, 20)";
    }

    public void reindex() {
        try {
            logger.info("Start pivot processing: "+PID);
            timestamp = currentUnixTime();

            Connection pivotsConn = getPivotsConnection();
            Statement stmt = pivotsConn.createStatement();
            String relationSql = getRelationSql();
            ResultSet rs = stmt.executeQuery(relationSql);

            logger.info("Building projects/forum index");
            int src_id, dst_id;
            while (rs.next()) {
                src_id = rs.getInt(1);
                dst_id = rs.getInt(2);
                if (!projectMap.containsKey(src_id)) {
                    projectMap.put(src_id, projectIndex.size());
                    projectIndex.add(src_id);
                }
                if (!forumMap.containsKey(dst_id)) {
                    forumMap.put(dst_id, forumIndex.size());
                    forumIndex.add(dst_id);
                }
            }

            logger.info("Project#: " + projectIndex.size());
            logger.info("Forum#: " + forumIndex.size());
            float[][] matrix = new float[projectIndex.size()][forumIndex.size()];
            rs.beforeFirst();
            while (rs.next()) {
                src_id = rs.getInt(1);
                dst_id = rs.getInt(2);
                matrix[projectMap.get(src_id)][forumMap.get(dst_id)] = getWeight(src_id, dst_id);
            }
            rs.close();
            stmt.close();
            pivotsConn.close();

            // calculate correlations, and save to database
            logger.info("Calculating matrix.");
            float[][] corMatrix = correlation(matrix);
            float score;
            pivotsConn = getPivotsConnection(); // reconnect to prevent timeout
            PreparedStatement insertStmt = pivotsConn.prepareStatement("insert into pivots_match(src_id, dest_id, pivot_id, score, a4)" +
                    "values (?, ?, "+PID+", ?, "+timestamp+")");
            logger.info("Writing results to database.");
            int projectSize = projectIndex.size();
            for (int i=0; i<projectSize; i++) {
                for (int j=0; j<projectSize; j++) {
                    score = corMatrix[i][j];
                    if (i==j || score<0.000001 || ((Float)score).isNaN()) continue; // we won't save src_id==dest_id, or correlation is negative value
                    try {
                        insertStmt.setInt(1, projectIndex.get(i));
                        insertStmt.setInt(2, projectIndex.get(j));
                        insertStmt.setFloat(3, score);
                        insertStmt.executeUpdate();
                    } catch (SQLException e) {
                        System.out.println("INSERT record error: "+projectIndex.get(i)+projectIndex.get(j)+score);
                    }
                }
            }
            insertStmt.close();

            pivotsConn.close();
            purgeObsolete(); // late purge to reduce offline time.
            logger.info("End pivot processing: "+PID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected float getWeight(int src_id, int dst_id) {
        return 1;
    }

    private void purgeObsolete() throws SQLException {
        Connection pivotsConn = getPivotsConnection();
        Statement stmt = pivotsConn.createStatement();
        stmt.executeUpdate("DELETE FROM pivots_match WHERE pivot_id="+PID+" AND a4<>"+ timestamp);
        stmt.close();
        closeConnection (pivotsConn);
    }

    /*private void outputRealMatrix(RealMatrix matrix) {
        try {
            PrintWriter file = new PrintWriter("cor.dat");
            for (int rowCounter=0; rowCounter<matrix.getRowDimension(); rowCounter++) {
                for (int columnCounter=0; columnCounter<matrix.getColumnDimension(); columnCounter++) {
                    file.printf("%6.4f\t", matrix.getEntry(rowCounter, columnCounter));
                }
                file.println();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
*/
    /*private RealMatrix computeCor(float[][] matrix) {
        try {
            MultivariateSummaryStatistics mss = new MultivariateSummaryStatistics(forumIndex.size(), true);
            for (int projectCounter=0; projectCounter<projectIndex.size(); projectCounter++) {
                mss.addValue(matrix[projectCounter]);
            }
            RealMatrix rm = mss.getCovariance();
            int n;
            if ((n=rm.getRowDimension()) != rm.getColumnDimension()) throw new RuntimeException();
            float[] diagVar = new float[n];
            for (int i=0; i<n; i++) {
                diagVar[i] = rm.getEntry(i, i);
            }

            float[][] corr = ((RealMatrixImpl)rm).getDataRef();
            for (int rowCounter=0; rowCounter<n; rowCounter++) {
                for (int columnCounter=0; columnCounter<n; columnCounter++) {
                    corr[rowCounter][columnCounter] = corr[rowCounter][columnCounter] / (Math.sqrt(diagVar[rowCounter]) * Math.sqrt(diagVar[columnCounter]));
                }
            }
            return rm;
        } catch (DimensionMismatchException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }*/

    // save the MatchMatrix to external file for other tasks.
    private void outputMatchMatrix(float[][] matrix) {
        try {
            PrintWriter file = new PrintWriter("data.dat");
            file.print("forumid\t");
            for (int projectId : projectIndex) {
                file.printf("p%d\t", projectId);
            }
            file.println();

            for (int forumCounter=0; forumCounter<forumIndex.size(); forumCounter++) {
                file.printf("f%d\t", forumIndex.get(forumCounter));
                for (int projectCounter=0; projectCounter<projectIndex.size(); projectCounter++) {
                    file.print((short)matrix[projectCounter][forumCounter]);
                    file.print('\t');
                }
                file.println();
            }
            file.flush();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void cleanup() {}

}
