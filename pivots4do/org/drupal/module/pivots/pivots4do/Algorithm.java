package org.drupal.module.pivots.pivots4do;

import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.sql.*;

// this is the one off program written just for drupal.org

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Feb 27, 2008
 */
public abstract class Algorithm {

    protected Logger logger;
    protected String options = "";
    protected int PID;

    public Algorithm() {
        if (logger==null) {
            logger = Logger.getLogger(this.getClass().getPackage().getName());
        }
        init();
    }

    public Connection getDrupalConnection() {
        return Console.getDrupalConnection();
    }
    public Connection getPivotsConnection() {
        return Console.getPivotsConnection();
    }
    public void closeConnection(Connection conn) {
        Console.closeConnection(conn);
    }

    public boolean tableExists(Connection conn, String tableName) {
        boolean exists = false;
        try {
            DatabaseMetaData metaData = conn.getMetaData();
            ResultSet rs = metaData.getTables(null, null, tableName, null);
            if (rs.next()) {
                exists = true;
            } else {
                exists = false;
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return exists;
    }

    public int currentUnixTime() {
        try {
            Connection conn = getDrupalConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT unix_timestamp()");
            rs.next();
            int utime = rs.getInt(1);
            rs.close();
            stmt.close();
            conn.close();
            return utime;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        //throw new IllegalStateException();
    }

    public abstract void init();
    public abstract void index();
    public abstract void reindex();
    public abstract void cleanup();

    /*public void cleanup() throws Exception {
        updateParameters();
        drupalConn.close();
        pivotsConn.close();
    }*/

    public void snapshot() throws SQLException {
        final int LIMIT = 20;
        Connection conn = getPivotsConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select sysdate()");
        rs.first();
        Timestamp snapshotTs = rs.getTimestamp(1);
        List<Integer> items = getSnapshotItems();
        logger.info("Taking snapshot of pivot-"+PID+" for "+items.size()+" items at "+snapshotTs);
        PreparedStatement selectItem = conn.prepareStatement("select dest_id, max(score) from pivots_match where src_id=? and pivot_id="+PID+" group by dest_id order by max(score) desc limit "+LIMIT);
        PreparedStatement insertItem = conn.prepareStatement("insert into pivots_snapshot(src_id, dest_id, pivot_id, score, snapshot_ts) value(?, ?, ?, ?, ?)");
        for (int itemId : items) {
            selectItem.setInt(1, itemId);
            rs = selectItem.executeQuery();
            while (rs.next()) {
                int destId = rs.getInt(1);
                double score = rs.getDouble(2);
                insertItem.setInt(1, itemId);
                insertItem.setInt(2, destId);
                insertItem.setInt(3, PID);
                insertItem.setDouble(4, score);
                insertItem.setTimestamp(5, snapshotTs);
                insertItem.executeUpdate();
            }
        }
    }

    public int updatedNumFromLastSnapshot() throws SQLException {
        Connection conn = getPivotsConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select max(snapshot_ts) from pivots_snapshot where pivot_id=" + PID);
        if (!rs.first()) return Integer.MAX_VALUE;
        Timestamp ts = rs.getTimestamp(1);
        logger.info("Last snapshot timestamp: "+rs);
        PreparedStatement pstmt = conn.prepareStatement("select count(*) from pivots_match where timestamp>? and pivot_id=" + PID);
        pstmt.setTimestamp(1, ts);
        rs = pstmt.executeQuery();
        if (!rs.first()) return Integer.MAX_VALUE;
        int num = rs.getInt(1);
        logger.info("Updated number from "+PID+": "+num);
        closeConnection(conn);
        return num;
    }

    protected List<Integer> getSnapshotItems() throws SQLException {
        List<Integer> items = new ArrayList<Integer>();
        Connection conn = getPivotsConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select distinct src_id from pivots_match where pivot_id="+PID);
        while (rs.next())  {
            items.add(rs.getInt(1));
        }
        return items;
    }
}
