package org.drupal.module.pivots.pivots4do;

import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.regex.Pattern;
import java.io.*;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Feb 27, 2008
 */
public class ForumAlgorithm extends Algorithm {

    protected int startMark;
    protected int endMark;

    public void init() {
        super.PID = 4151;
        try {
            Connection pivotsConn = getPivotsConnection();
            Statement stmt = pivotsConn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT states FROM pivots_external WHERE pid="+PID);
            if (rs.first()) {
                // retrieve original parameters
                Properties param = new Properties();
                param.load(new StringReader(rs.getString(1)));
                startMark = Integer.parseInt(param.getProperty("startMark"));
            } else {
                // init parameters from scratch
                startMark = 0;
                Statement stmt1 = pivotsConn.createStatement();
                stmt1.executeUpdate("INSERT INTO pivots_external(pid) VALUES ("+PID+")");
            }
            closeConnection(pivotsConn);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (NumberFormatException e) {
            startMark=0;
        }
    }

    protected abstract class ProjectNode implements Comparable<ProjectNode> {
        int nid;
        String title;
        int changed;
        abstract Pattern getPattern();
        abstract int getSignature();
        public boolean isBlacklisted() {
            return projectBlacklist.contains(nid);
        }
        public int compareTo(ProjectNode pn) {
            if (this.getSignature() > pn.getSignature()) {
                return -1; // alias is smaller than module/theme
            } else if (this.getSignature() < pn.getSignature()) {
                return 1;
            } else { // signature is the same
                if (this.title.length() > pn.title.length()) {
                    return 1; // title length is longer, then bigger
                } else if (this.title.length() < pn.title.length()) {
                    return -1;
                } else {
                    return this.title.compareTo(pn.title);
                }
            }
        }
    }

    public static final int MODULE_SIGNATURE = 10;
    public static final int THEME_SIGNATURE = 15;
    // this used to be 15 (maybe a typo). now changed to 20
    public static final int ALIAS_SIGNATURE = 20;

    protected class ModuleNode extends ProjectNode{
        Pattern getPattern() {
            String title = Pattern.quote(this.title);
            return Pattern.compile(
                    "(?:\\b" + title + "\\b\\W+?\\bmodule\\b)|(?:\\bmodule\\b\\W+?\\b" + title + "\\b)",
                    Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        }
        int getSignature() {
            return MODULE_SIGNATURE;
        }
    }

    protected class ThemeNode extends ProjectNode {
        Pattern getPattern() {
            String title = Pattern.quote(this.title);
            return Pattern.compile(
                    "(?:\\b" + title + "\\b\\W+?\\btheme\\b)|(?:\\btheme\\b\\W+?\\b" + title + "\\b)",
                    Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        }
        int getSignature() {
            return THEME_SIGNATURE;
        }
    }

    protected class AliasNode extends ProjectNode {
        Pattern getPattern() {
            if (title.startsWith("^")) {
                // strip the first ^ character and do the case sensitive regex search
                return Pattern.compile("\\b"+Pattern.quote(title.substring(1))+"\\b", Pattern.DOTALL);
            } else {
                // do the case insenstive regex search
                return Pattern.compile("\\b"+Pattern.quote(title)+"\\b", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
            }
        }
        int getSignature() {
            return ALIAS_SIGNATURE;
        }
    }

    protected class MessageNode implements Cloneable {
        int nid;
        int cid;
        String subject;
        String body;
        int changed;
    }

    private class Match {
        int srcId;
        int destId;
        int extraId;
        int signature; // module->10, theme->15, alias->20
        float score;
        Match() {};
        Match(int srcId, int destId, int extraId, int signature, float score) {
            this.srcId = srcId;
            this.destId = destId;
            this.extraId = extraId;
            this.signature = signature;
            this.score = score;
        }
        String toValueString() {
            StringBuilder sb = new StringBuilder();
            sb.append('(').append(srcId).append(',').append(destId).append(',').append(PID).append(',').append(extraId).append(',').append(signature).append(',').append(score).append(')');
            return sb.toString();
        }
    }

    protected List<ProjectNode> newProjectList = new ArrayList<ProjectNode>();
    protected List<ProjectNode> oldProjectList = new ArrayList<ProjectNode>();
    protected List<ProjectNode> allProjectList = new ArrayList<ProjectNode>();
    protected List<MessageNode> newMessageList = new ArrayList<MessageNode>();
    protected List<MessageNode> oldMessageList = new ArrayList<MessageNode>();
    protected List<MessageNode> allMessageList = new ArrayList<MessageNode>();
    //protected Queue<Match> matchQueue = new ConcurrentLinkedQueue<Match>();
    protected List<Match> matchList = new ArrayList<Match>();
    // It's nuch better to skip the projects when generating projectList.
    // This is just minimize change to existing code. Same thing for the redundant matchSet.
    protected Set<Integer> projectBlacklist = new HashSet<Integer>();
    protected Map<Integer, ProjectProbe> allProjectProbeMap = new HashMap<Integer, ProjectProbe>();
    protected Map<Integer, ProjectProbe> newProjectProbeMap = new HashMap<Integer, ProjectProbe>();
    protected Map<Integer, ProjectProbe> oldProjectProbeMap = new HashMap<Integer, ProjectProbe>();

    public void index() {
        logger.warning("Pivot index processing start time: " + new Date());
        endMark = currentUnixTime();
        /*final int days360 = 60*60*24*360;
        if (!isExtensiveMode() && (endMark - startMark)>days360) {
            if (startMark==0) {
                endMark = 1136091600; // unix timestamp for 2006-01-01
            } else {
                endMark = startMark + days360;  
            }
            logger.warning("Too long timespan. Switch to end unixtime: " + endMark);
            logger.warning("Please run incremental index again to process the rest of the data.");
        }*/
        logger.info("Start Mark: " + startMark + "\nEnd Mark: " + endMark);
        buildBlacklist();
        populateList();
        purgeObsolete();
        matching();
        saveMatchesToDatabase();
    }

    private void buildBlacklist() {
        try {
            // NOTE: add code that generates blacklist, or manually generate a blacklist.
            Connection pivotsConn = getPivotsConnection();
            Statement stmt = pivotsConn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT nid FROM pivots_blacklist");
            while (rs.next()) {
                projectBlacklist.add(rs.getInt(1));
            }
            rs.close();
            stmt.close();
            closeConnection(pivotsConn);
            logger.info("Blacklist#: "+projectBlacklist.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void cleanup() {
        try {
            startMark = endMark;
            Properties param = new Properties();
            param.setProperty("startMark", (new Integer(startMark)).toString());
            StringWriter writer = new StringWriter();
            param.store(writer, null);
            
            Connection pivotsConn = getPivotsConnection();
            PreparedStatement stmt = pivotsConn.prepareStatement("UPDATE pivots_external SET states=? WHERE pid="+PID);
            stmt.setString(1, writer.toString());
            stmt.executeUpdate();
            stmt.close();
            closeConnection(pivotsConn);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void saveMatchesToDatabase() {
        if (matchList.size()<=0) return;
        try {
            logger.info("Preparing write results to database.");
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO pivots_match(src_id, dest_id, pivot_id, a1, a2, score) VALUES ");
            for (Match match : matchList) {
                sb.append(match.toValueString()).append(',');
            }
            sb.deleteCharAt(sb.length()-1); // delete the last ","

            // backup.don't know whether sql that large is permitted.
            try {
                FileWriter writer = new FileWriter("insert.sql");
                writer.write(sb.toString());
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning("SQL not saved to file.");
            }

            Connection pivotsConn = getPivotsConnection();
            Statement stmt = pivotsConn.createStatement();
            stmt.executeUpdate(sb.toString());
            stmt.close();
            closeConnection(pivotsConn);
            logger.info("Finish writing results to database");

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void matching() {
        //step1: all projects and new messages
        //doMatching(allProjectList, newMessageList);
        doMatching(allProjectProbeMap, newMessageList);

        //step2: old messages and new projects
        if (isExtensiveMode()) {
            //doMatching(newProjectList, oldMessageList);
            doMatching(newProjectProbeMap, oldMessageList);
        }

        logger.info("Total results#: " + matchList.size());
    }

    private void doMatching(Map<Integer, ProjectProbe> projectProbeMap, List<MessageNode> messageList) {
        Collection<ProjectProbe> projectProbeList = projectProbeMap.values();
        int count = 1;
        int size = projectProbeList.size();
        for (ProjectProbe projectProbe : projectProbeList) {
            List<ProjectNode> probes = projectProbe.getProbes();
            logger.info("Processing "+(count++)+"/"+size+" project: "+projectProbe.nid+" ("+projectProbe.toString()+")");
            for (MessageNode messageNode : messageList) {
                for (ProjectNode projectNode : probes) {
                    Pattern pattern = projectNode.getPattern();
                    if (pattern.matcher(messageNode.subject).find() || pattern.matcher(messageNode.body).find()) {
                        addMatchList(projectNode, messageNode); // TODO: probably add other info, such as how many matches in a single message.
                        break; // skip the rest of the probes for the message. eg. if we find cck, then no need to find 'Content Construction Kits'
                    }
                }
            }
        }
    }

    /*private void doMatching(List<ProjectNode> projectList, List<MessageNode> messageList) {
        int count = 1;
        int size = projectList.size();
        for (ProjectNode projectNode : projectList) {
            logger.info("Processing "+(count++)+"/"+size+" project: "+projectNode.nid);
            if (projectNode.isBlacklisted()) {
                logger.info("Skip project: "+projectNode.nid);
                continue;
            }
            Pattern pattern = projectNode.getPattern();
            for (MessageNode messageNode : messageList) {
                if (pattern.matcher(messageNode.subject).find() || pattern.matcher(messageNode.body).find()) {
                    addMatchList(projectNode, messageNode); // TODO: probably add other info, such as how many matches in a single message.
                }
            }
        }
    }*/

    private void addMatchList(ProjectNode projectNode, MessageNode messageNode) {
        // BUG: we haven't eliminate the duplicates
        matchList.add(new Match(projectNode.nid, messageNode.nid, messageNode.cid, projectNode.getSignature(), messageNode.changed));
        matchList.add(new Match(messageNode.nid, projectNode.nid, messageNode.cid, projectNode.getSignature()+1, messageNode.changed));
    }

    private void purgeObsolete() {
        // TODO: add code
    }

    private void populateList() {
        try {
            Connection drupalConn = getDrupalConnection();
            Connection pivotsConn = getPivotsConnection();
            logger.info("Start reading data from the database.");

            populateNewProjectList(drupalConn, pivotsConn);
            populateOldProjectList(drupalConn, pivotsConn);
            allProjectList.addAll(newProjectList);
            allProjectList.addAll(oldProjectList);

            buildProjectProbeMap(newProjectProbeMap, newProjectList);
            buildProjectProbeMap(oldProjectProbeMap, oldProjectList);
            buildProjectProbeMap(allProjectProbeMap, allProjectList);

            populateNewMessageList(drupalConn);
            allMessageList.addAll(newMessageList);
            if (isExtensiveMode()) {
                populateOldMessageList(drupalConn);
                allMessageList.addAll(oldMessageList);
            }


            logger.info("Finish reading data from the database.");
            closeConnection(drupalConn);
            closeConnection(pivotsConn);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void buildProjectProbeMap(Map<Integer, ProjectProbe> projectProbeMap, List<ProjectNode> projectList) {
        for (ProjectNode pn : projectList) {
            if (pn.isBlacklisted()) continue;
            if (projectProbeMap.containsKey(pn.nid)) {
                ProjectProbe pp = projectProbeMap.get(pn.nid);
                pp.add(pn);
            } else {
                ProjectProbe pp = new ProjectProbe();
                pp.add(pn);
                projectProbeMap.put(pn.nid, pp);
            }
        }
    }

    private void populateOldMessageList(Connection drupalConn) throws SQLException {
        // old message
        Statement oldMessageStatement = drupalConn.createStatement();
        ResultSet oldMessageResultSet = oldMessageStatement.executeQuery("SELECT n.nid, 0 cid, n.changed changed, n.title subject, r.body body FROM node n INNER JOIN node_revisions r " +
            "ON n.nid=r.nid AND n.vid=r.vid WHERE n.type='forum' AND n.changed<="+startMark+" UNION ALL " +
            "SELECT c.nid, c.cid, c.timestamp changed, c.subject subject, c.comment body FROM comments c INNER JOIN node n " +
            "ON c.nid=n.nid WHERE n.type='forum' AND c.timestamp<="+startMark);
        while (oldMessageResultSet.next()) {
            MessageNode messageNode = new MessageNode();
            messageNode.nid = oldMessageResultSet.getInt(1);
            messageNode.cid = oldMessageResultSet.getInt(2);
            messageNode.changed = oldMessageResultSet.getInt(3);
            messageNode.subject = oldMessageResultSet.getString(4);
            messageNode.body = oldMessageResultSet.getString(5);
            oldMessageList.add(messageNode);
        }
        oldMessageResultSet.close();
        oldMessageStatement.close();
        logger.info("Old Messages#: " + oldMessageList.size());
    }

    private void populateNewMessageList(Connection drupalConn) throws SQLException {
        // new message
        Statement newMessageStatement = drupalConn.createStatement();
        ResultSet newMessageResultSet = newMessageStatement.executeQuery("SELECT n.nid, 0 cid, n.changed changed, n.title subject, r.body body FROM node n INNER JOIN node_revisions r " +
            "ON n.nid=r.nid AND n.vid=r.vid WHERE n.type='forum' AND n.changed>"+startMark+" AND n.changed<="+endMark+" UNION ALL " +
            "SELECT c.nid, c.cid, c.timestamp changed, c.subject subject, c.comment body FROM comments c INNER JOIN node n " +
            "ON c.nid=n.nid WHERE n.type='forum' AND c.timestamp>"+startMark+" AND c.timestamp<="+endMark);
        while (newMessageResultSet.next()) {
            MessageNode messageNode = new MessageNode();
            messageNode.nid = newMessageResultSet.getInt(1);
            messageNode.cid = newMessageResultSet.getInt(2);
            messageNode.changed = newMessageResultSet.getInt(3);
            messageNode.subject = newMessageResultSet.getString(4);
            messageNode.body = newMessageResultSet.getString(5);
            newMessageList.add(messageNode);
        }
        newMessageResultSet.close();
        newMessageStatement.close();
        logger.info("New Messages#: " + newMessageList.size());
    }

    private void populateOldProjectList(Connection drupalConn, Connection pivotsConn) throws SQLException {
        // old project
        Statement oldProjectStatement = drupalConn.createStatement();
        ResultSet oldProjectResultSet = oldProjectStatement.executeQuery("SELECT n.nid, n.title, n.changed, TRIM(LEADING '/' FROM SUBSTRING_INDEX(p.directory, '/', 2)) AS ptype " +
                "FROM node n INNER JOIN cvs_projects p ON n.nid=p.nid WHERE p.rid=2 AND n.type='project_project' " +
                "AND (p.directory LIKE '/modules/%' OR p.directory LIKE '/themes/%') " +
                "AND changed<="+startMark); // this is the only place that is different from the previous one.
        while (oldProjectResultSet.next()) {
            String ptype = oldProjectResultSet.getString(4);
            ProjectNode projectNode;
            if (ptype.equals("modules")) {
                projectNode = new ModuleNode();
            } else if (ptype.equals("themes")) {
                projectNode = new ThemeNode();
            } else {
                continue;
            }
            projectNode.nid = oldProjectResultSet.getInt(1);
            projectNode.title = oldProjectResultSet.getString(2);
            projectNode.changed = oldProjectResultSet.getInt(3);
            oldProjectList.add(projectNode);
        }
        oldProjectResultSet.close();
        oldProjectStatement.close();
        // old alias
        Statement oldAliasStatement = pivotsConn.createStatement();
        ResultSet oldAliasResultSet = oldAliasStatement.executeQuery("SELECT nid, alias, changed FROM pivots_alias WHERE changed<="+startMark);
        while (oldAliasResultSet.next()) {
            ProjectNode projectNode = new AliasNode();
            projectNode.nid = oldAliasResultSet.getInt(1);
            projectNode.title = oldAliasResultSet.getString(2);
            projectNode.changed = oldAliasResultSet.getInt(3);
            oldProjectList.add(projectNode);
        }
        oldAliasResultSet.close();
        oldAliasStatement.close();
        logger.info("Old Project#: " + oldProjectList.size());
    }

    private void populateNewProjectList(Connection drupalConn, Connection pivotsConn) throws SQLException {
        // new project
        Statement newProjectStatement = drupalConn.createStatement();
        ResultSet newProjectResultSet = newProjectStatement.executeQuery("SELECT n.nid, n.title, n.changed, TRIM(LEADING '/' FROM SUBSTRING_INDEX(p.directory, '/', 2)) AS ptype " +
                "FROM node n INNER JOIN cvs_projects p ON n.nid=p.nid WHERE p.rid=2 AND n.type='project_project' " +
                "AND (p.directory LIKE '/modules/%' OR p.directory LIKE '/themes/%') " +
                "AND changed>"+startMark+" AND changed<="+endMark);
        while (newProjectResultSet.next()) {
            String ptype = newProjectResultSet.getString(4);
            ProjectNode projectNode;
            if (ptype.equals("modules")) {
                projectNode = new ModuleNode();
            } else if (ptype.equals("themes")) {
                projectNode = new ThemeNode();
            } else {
                continue;
            }
            projectNode.nid = newProjectResultSet.getInt(1);
            projectNode.title = newProjectResultSet.getString(2);
            projectNode.changed = newProjectResultSet.getInt(3);
            newProjectList.add(projectNode);
        }
        newProjectResultSet.close();
        newProjectStatement.close();
        // new alias
        Statement newAliasStatement = pivotsConn.createStatement();
        ResultSet newAliasResultSet = newAliasStatement.executeQuery("SELECT nid, alias, changed FROM pivots_alias WHERE changed>"+startMark+" AND changed<="+endMark);
        while (newAliasResultSet.next()) {
            ProjectNode projectNode = new AliasNode();
            projectNode.nid = newAliasResultSet.getInt(1);
            projectNode.title = newAliasResultSet.getString(2);
            projectNode.changed = newAliasResultSet.getInt(3);
            newProjectList.add(projectNode);
        }
        newAliasResultSet.close();
        newAliasStatement.close();
        logger.info("New Project#: " + newProjectList.size());
    }

    public void reindex() {
        try {
            Connection pivotsConn = getPivotsConnection();
            Statement stmt = pivotsConn.createStatement();
            stmt.executeUpdate("DELETE FROM pivots_match WHERE pivot_id="+PID);
            stmt.close();
            closeConnection(pivotsConn);
            startMark = 0;
            logger.info("Re-indexing.");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        index();
    }

    private boolean isExtensiveMode() {
        if (options==null) return false;
        if (options.indexOf("extensive")>=0) {
            logger.info("Running in Extensive Mode.");
            return true;
        } else {
            return false;
        }
    }

    protected List<Integer> getSnapshotItems() throws SQLException {
        List<Integer> items = new ArrayList<Integer>();
        Connection conn = getPivotsConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select distinct src_id from pivots_match where pivot_id="+PID+" and a2 in (10,15)");
        while (rs.next())  {
            items.add(rs.getInt(1));
        }
        return items;
    }

    public boolean isSkippable() {
        int now = currentUnixTime();
        int hours = (now - startMark) / 3600;
        return hours==0;
    }

    private class ProjectProbe {
        private List<ProjectNode> probes = new ArrayList<ProjectNode>();
        protected int nid;

        public void add(ProjectNode newNode) {
            if (probes.size()==0) {
                nid = newNode.nid;
            } else {
                if (newNode.nid != nid) throw new IllegalArgumentException();
            }
            // remove duplicate for AliasNode. logic is a little messed up :P
            if (newNode.getSignature() == ALIAS_SIGNATURE) {
                Iterator<ProjectNode> iter = probes.iterator();
                ProjectNode existingNode = null;
                while (iter.hasNext()) {
                    existingNode = iter.next();
                    if (existingNode.title.toLowerCase().indexOf(newNode.title.toLowerCase()) >=0) { // newNode.title is a substring of existingNode.title
                        // this could even remove the Module/Theme node if the alias has the same title.
                        iter.remove();
                    } else if (newNode.title.toLowerCase().indexOf(existingNode.title.toLowerCase())>=0 && existingNode.getSignature()==ALIAS_SIGNATURE) {
                        // existingNode.title is a substring of newNode.title.
                        // don't add newNode.
                        return;
                    }
                }
            } else {
                for (ProjectNode existingNode : probes) {
                    if (newNode.title.toLowerCase().indexOf(existingNode.title.toLowerCase())>=0 && existingNode.getSignature()==ALIAS_SIGNATURE) {
                        // existingNode.title (an alias) is a substring of newNode.title (a Module/Theme node).
                        // don't add newNode.
                        return;
                    }
                }
            }
            probes.add(newNode);
            Collections.sort(probes);
        }

        public List<ProjectNode> getProbes() {
            return probes;
        }
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(probes.size()).append('|');
            for (ProjectNode pn : probes) {
                sb.append(pn.title).append('|');
            }
            return sb.toString();
        }
    }
}
