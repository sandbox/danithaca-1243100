package org.drupal.module.pivots.pivots4do;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Feb 4, 2009
 */
public class ProjectUsageAlgorithm extends DoubleCorrAlgorithm {
    public void init() {
        super.PID = 4157;
    }

    protected String getRelationSql() {
        return "select pid, sid from project_usage";
    }
}
