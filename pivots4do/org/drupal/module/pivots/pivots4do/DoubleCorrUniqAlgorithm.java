package org.drupal.module.pivots.pivots4do;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Oct 27, 2008
 */
public class DoubleCorrUniqAlgorithm extends DoubleCorrAlgorithm {
    Connection conn;
    PreparedStatement stmt;

    public void init() {
        try {
            super.PID = 4156;
            conn = getPivotsConnection();
            stmt = conn.prepareStatement("SELECT count(distinct dest_id) num FROM pivots_match WHERE pivot_id=4151 AND src_id=?");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("database error");
        }
    }

    public void reindex() {
        super.reindex();
        try {
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println("DB connection close error. Ignore.");
        }
    }

    protected float getWeight(int src_id, int dst_id) {
        float weight = 0;
        try {
            stmt.setInt(1, dst_id);
            ResultSet rs = stmt.executeQuery();
            rs.first();
            int num = rs.getInt(1);
            weight = 1.0f / num;
        } catch (SQLException e) {
            System.err.println("Node not exists: "+dst_id);
        }
        return weight;
    }
}
