package org.drupal.module.pivots.pivots4do;

import java.util.Properties;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.net.URI;
import java.net.URISyntaxException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;

public class Console {

    static final String VERSION = "2.6.2";
    static String drupalDbUrl;
    static String pivotsDbUrl;

    public static void main(String[] args) throws Exception {
        System.out.println("PIVOTS FOR D.O.VERSION: " + VERSION);
        setup();
        if (args.length == 1 && args[0].equalsIgnoreCase("cron")) {
            cron();
        } else {
            runPivot(args);
        }
        System.out.println("Pivot indexing accomplished.");
    }

    public static void cron() throws Exception {
        System.out.println("Running in CRON mode!");
        ForumAlgorithm forumPivot = new ForumAlgorithm();
        if (forumPivot.isSkippable()) {
            System.out.println("Too soon to run conversation pivots again. Exit.");
            return;
        }
        forumPivot.index();
        forumPivot.cleanup();
        if (shouldRunDouble()) {
            forumPivot.snapshot();
            Algorithm corrPivot = new DoubleCorrAlgorithm();
            corrPivot.index();
            corrPivot.cleanup();
            corrPivot.snapshot();
        }
    }

    protected static Connection getDrupalConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(drupalDbUrl);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return conn;
    }
    protected static Connection getPivotsConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(pivotsDbUrl);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return conn;
    }
    protected static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    private static void setup() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        Properties phpVars = readSettingsPhp();
        drupalDbUrl = convertDbUrl(phpVars.getProperty("drupal_db_url"));
        pivotsDbUrl = convertDbUrl(phpVars.getProperty("pivots_db_url"));
    }

    private static void runPivot(String... args) {
        Algorithm pivot;
        if (args[0].equalsIgnoreCase("forum")) {
            pivot = new ForumAlgorithm();
        } else if (args[0].equalsIgnoreCase("double")) {
            pivot = new DoubleAlgorithm();
        } else if (args[0].equalsIgnoreCase("corr")) {
            pivot = new DoubleCorrAlgorithm();
        } else if (args[0].equalsIgnoreCase("recency")) {
            pivot = new DoubleCorrRecencyAlgorithm();
        } else if (args[0].equalsIgnoreCase("uniq")) {
            pivot = new DoubleCorrUniqAlgorithm();
        } else if (args[0].equalsIgnoreCase("usage")) {
            pivot = new ProjectUsageAlgorithm();
        } else {
            throw new RuntimeException("No supported pivots algorithm.");
        }
        if (args.length>1) {
            // set the options
            if (args.length>2) pivot.options = args[2];
            if (args[1].equalsIgnoreCase("--reindex")) {
                pivot.reindex();
            } else if (args[1].equalsIgnoreCase("--index")) {
                pivot.index();
            } else {
                throw new RuntimeException("Illegal operation;");
            }
        }
        pivot.cleanup();
    }

    static String convertDbUrl(String phpUrl) {
        try {
            StringBuilder sb = new StringBuilder();
            URI uri = new URI(phpUrl);
            String user = (uri.getUserInfo().split(":"))[0];
            String password = (uri.getUserInfo().split(":"))[1];
            sb.append("jdbc:mysql://").append(uri.getHost()).append(uri.getPath()).append("?user=").append(user).append("&password=").append(password).append("&autoReconnect=true");
            return sb.toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static Properties readSettingsPhp() {
        try {
            Properties vars = new Properties();
            //Pattern phpVar = Pattern.compile("\\s*\\$(\\w+?)\\s*=(?:(?:\\s*\"(.*)\"\\s*)|(?:\\s*'(.*)'\\s*))");
            Pattern phpVar = Pattern.compile("\\s*\\$(\\w+?)\\s*=\\s*[\"'](.*)[\"']\\s*;\\s*");
            BufferedReader settingsPhp = new BufferedReader(new FileReader("settings.php"));
            String line;
            while ((line=settingsPhp.readLine()) != null) {
                Matcher matcher = phpVar.matcher(line);
                if (matcher.matches()) {
                    vars.setProperty(matcher.group(1), matcher.group(2));
                }
            }
            settingsPhp.close();
            return vars;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static boolean shouldRunDouble() {
        try {
            Connection conn = getPivotsConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select max(snapshot_ts) from pivots_snapshot where pivot_id=4151");
            rs.first();
            Timestamp ts = rs.getTimestamp(1);
            if (ts==null || ts.getTime()<=0) return true;
            System.out.println("Last snapshot timestamp: "+ts);
            PreparedStatement pstmt = conn.prepareStatement("select count(distinct src_id, dest_id) from pivots_match where timestamp>? and pivot_id=4151 and a2 in (10, 15)");
            pstmt.setTimestamp(1, ts);
            rs = pstmt.executeQuery();
            rs.first();
            int num = rs.getInt(1);
            System.out.println("Updated forum posts: "+num);
            closeConnection(conn);
            if (num>=500) return true;
            else return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
