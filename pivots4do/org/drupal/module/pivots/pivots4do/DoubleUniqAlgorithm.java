package org.drupal.module.pivots.pivots4do;

import java.sql.*;
import java.util.*;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Sep 23, 2008
 */
public class DoubleUniqAlgorithm extends Algorithm{
    // this is the pid we want to get the modules that are at least positively correlated
    private static final int PID_RELEVENCY = 4153;
    // this is the pid we want to get the # of co-references and total references
    private static final int PID_REF = 4152;

    // this is the PID of this algorithm.
    protected int timestamp;

    public void init() {
        super.PID=4155;
    }

    public void index() {
        logger.info("Attention: the current implementation will do full reindex in an incremental index cycle");
        reindex();
    }

    public void reindex() {
        try {
            System.out.println("Please make sure the CoReferences pivot "+PID_REF+" and the Relevency pivot "+PID_RELEVENCY+" has the latest data.");
            logger.info("Start pivot processing: " + PID);
            timestamp = currentUnixTime();

            Connection pivotsConn = getPivotsConnection();
            // store the total references of each project node into a map
            Statement stmtTotalRef = pivotsConn.createStatement();
            ResultSet rsTotalRef = stmtTotalRef.executeQuery("select src_id, score from pivots_match where src_id=dest_id and pivot_id="+PID_REF);
            Map<Integer, Double> refMap = new HashMap<Integer, Double>(2000);
            while (rsTotalRef.next()) {
                refMap.put(rsTotalRef.getInt(1), rsTotalRef.getDouble(2));
            }
            rsTotalRef.close();
            stmtTotalRef.close();

            // should return only one value
            PreparedStatement stmtCoref = pivotsConn.prepareStatement("select score from pivots_match where src_id=? and dest_id=? and pivot_id="+PID_REF);
            ResultSet rsCoref;
            PreparedStatement stmtInsert = pivotsConn.prepareStatement("insert pivots_match(pivot_id, src_id, dest_id, score, a4) values ("+PID+", ?, ?, ?, "+timestamp+")");

            // from the eligible relations (relvency>0), get the uniqueness index, and save to database
            Statement stmtRelevency = pivotsConn.createStatement();
            ResultSet rsRelevency = stmtRelevency.executeQuery("select src_id, dest_id from pivots_match where src_id!=dest_id and pivot_id="+PID_RELEVENCY);
            int src_id, dest_id;
            double coref, totalref, score;
            while (rsRelevency.next()) {
                src_id = rsRelevency.getInt(1);
                dest_id = rsRelevency.getInt(2);
                stmtCoref.setInt(1, src_id);
                stmtCoref.setInt(2, dest_id);
                rsCoref = stmtCoref.executeQuery();
                // we should get at most 1 result
                if (rsCoref.first()) {
                    coref = rsCoref.getDouble(1);
                    rsCoref.close();
                } else {
                    System.err.println("Cannot find co-ref:"+src_id+"<-->"+dest_id);
                    continue;
                }
                // if we don't get the total ref, it's a problem
                if (!refMap.containsKey(dest_id)) {
                    System.err.println("Cannot find total ref:"+dest_id);
                    continue;
                }
                totalref = refMap.get(dest_id);
                // calculate uniqueness score
                score = coref / totalref;
                // insert an record.
                stmtInsert.setInt(1, src_id);
                stmtInsert.setInt(2, dest_id);
                stmtInsert.setDouble(3, score);
                stmtInsert.executeUpdate();
            }
            rsRelevency.close();
            stmtRelevency.close();
            stmtInsert.close();
            stmtCoref.close();

            pivotsConn.close();
            purgeObsolete(); // late purge to reduce offline time.
            logger.info("End pivot processing: "+PID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void cleanup() {                             
        
    }

    private void purgeObsolete() throws SQLException {
        Connection pivotsConn = getPivotsConnection();
        Statement stmt = pivotsConn.createStatement();
        stmt.executeUpdate("DELETE FROM pivots_match WHERE pivot_id="+PID+" AND a4<>"+ timestamp);
        stmt.close();
        closeConnection (pivotsConn);
    }
}
