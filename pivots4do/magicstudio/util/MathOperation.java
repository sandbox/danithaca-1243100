package magicstudio.util;

/**
 * Math tools. Could be copied to other sources.
 */
public class MathOperation {

    public static float mean(float[] vector) {
        if (vector==null || vector.length==0) throw new IllegalArgumentException();
        float result = 0;
        for (int i=0; i<vector.length; i++) {
            result += vector[i];
        }
        return result/vector.length;
    }

    public static float covariance(float[] vectorA, float[] vectorB) {
        if (vectorA==null || vectorA.length==0 || vectorB==null || vectorB.length==0) throw new IllegalArgumentException();
        if (vectorA.length != vectorB.length) throw new IllegalArgumentException("Two vectors need to have the same dimension");
        int n = vectorA.length;
        float meanA = mean(vectorA);
        float meanB = mean(vectorB);
        float[] variance = new float[n];
        for (int i=0; i<n; i++) {
            variance[i] = (vectorA[i]-meanA) * (vectorB[i]-meanB);
        }
        return mean(variance);
    }

    /**
     * @param matrix Matrix[m][n] is an m by n matrix. There should be m vectors; each vector has n values.
     * @return the m*m covariance matrix.
     */
    public static float[][] covariance(float[][] matrix) {
        if (matrix==null || matrix.length==0) throw new IllegalArgumentException();
        int m = matrix.length; // # of vectors
        int n = matrix[0].length; // # of obs in each vectors. should be a constant in each vector.
        float[][] covMatrix = new float[m][m];

        float[] vector1;
        float[] vector2;
        float cov;
        // v1 is the first vector in the covariance formular
        for (int v1=0; v1<m; v1++) {
            // v2 is the second vector in the covariance formular
            for (int v2=v1; v2<m; v2++) {
                vector1 = matrix[v1];
                vector2 = matrix[v2];
                if (vector1.length!=n || vector2.length!=n) throw new IllegalArgumentException();
                cov = covariance(vector1, vector2);
                covMatrix[v1][v2] = cov;
                covMatrix[v2][v1] = cov;
            }
        }
        return covMatrix;
    }

    /**
     * @param matrix Matrix[m][n] is an m by n matrix. There should be m vectors; each vector has n values.
     * @return The correlation matrix[m][m]
     */
    public static float[][] correlation(float matrix[][]) {
        float[][] covMatrix = covariance(matrix);
        int m = covMatrix.length;
        float[] vectorDiagonal = new float[m];
        for (int i=0; i<m; i++) {
            if (covMatrix[i].length!=m) throw new IllegalArgumentException();
            vectorDiagonal[i] = (float)Math.sqrt(covMatrix[i][i]);
        }
        float[][] corMatrix = covMatrix; // we use the shared matrix in order to save memory
        for (int v1=0; v1<m; v1++) {
            for (int v2=v1; v2<m; v2++) {
                corMatrix[v1][v2] = covMatrix[v1][v2] / (vectorDiagonal[v1] * vectorDiagonal[v2]);
                corMatrix[v2][v1] = corMatrix[v1][v2];
            }
        }
        return corMatrix;
    }

    /**
     * To test whether two float numbers are approximately equal.
     * Relative error < 0.0001
     */
    public static boolean approxEquals(float a, float b) {
        final float TOLERANCE = 0.0001f;
        if (a==b) return true;
        if (a==0d || b==0d) return false;
        float error = Math.abs(b-a);
        return Math.abs(error/a)<=TOLERANCE && Math.abs(error/b)<=TOLERANCE;
    }
}
