package edu.umich.si.cms.pivots.perl;

import org.drupal.module.pivots.pivots4do.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Jan 23, 2009
 */
public abstract class PerlAlgorithm extends Algorithm {

    private final String dbUrl = "jdbc:mysql://zeno.si.umich.edu/annocpan2?user=remote&password=w01verine&&autoReconnect=true";

    public Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(dbUrl);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return conn;
    }

    public Connection getDrupalConnection() {
        return getConnection();
    }

    public Connection getPivotsConnection() {
        return getConnection();
    }

    public void reindex_proxy() throws SQLException {
        System.out.println("Cleaning up.");
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute("create temporary table modules select distinct modulename from perlmonkindex2 where modulename not in (select mname from pivots_module)");
        stmt.execute("insert into pivots_module(mname) select modulename from modules");
        stmt.execute("delete from pivots_match where pivot_id=4151");
        stmt.execute("insert into pivots_match(src_id, dest_id, pivot_id, score, a2) select mid, root_node, 4151, 0, 10 from pivots_module m inner join perlmonkindex2 i on m.mname=i.modulename");
        closeConnection(conn);
        reindex();
    }

    public static void main(String[] args) throws Exception {
        // initialize connection pooling (no connection pooling temporarily)
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        // instantiate algorithms
        PerlAlgorithm pivot;
        if (args[0].equalsIgnoreCase("corr")) {
            pivot = new CorrPerlAlgorithm();
        } else if (args[0].equalsIgnoreCase("double")) {
            pivot = new DoublePerlAlgorithm();
        } else if (args[0].equalsIgnoreCase("recency")) {
            pivot = new RecencyPerlAlgorithm();
        } else if (args[0].equalsIgnoreCase("uniq")) {
            pivot = new UniqPerlAlgorithm();
        } else {
            throw new RuntimeException("No supported pivots algorithm.");
        }

        pivot.reindex_proxy();
        pivot.cleanup();
        System.out.println("Process accomplished.");
    }
}
