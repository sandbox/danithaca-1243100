package edu.umich.si.cms.pivots.perl;

import org.drupal.module.pivots.pivots4do.Algorithm;
import org.drupal.module.pivots.pivots4do.ForumAlgorithm;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;

import java.sql.*;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Feb 27, 2008
 */
public class DoublePerlAlgorithm extends PerlAlgorithm {

    protected static final int PID = 4152;
    protected int basePid = 4151;
    protected int timestamp;

    public void init() {

    }

    public void index() {
        logger.info("Attention: the current implementation will do full reindex in an incremental index cycle");
        reindex();
    }

    public void reindex() {
        try {
            logger.info("Start double pivot processing.");
            timestamp = currentUnixTime();

            Connection pivotsConn = getPivotsConnection();
            String tableName = "_pivots_matchx";
            Statement stmt = pivotsConn.createStatement();
            stmt.executeUpdate("CREATE TEMPORARY TABLE IF NOT EXISTS " + tableName +
                    " (src_id INT(11) UNSIGNED NOT NULL, dest_id INT(11) UNSIGNED NOT NULL)" +
                    " SELECT DISTINCT src_id, dest_id FROM pivots_match m WHERE pivot_id="+basePid +
                    " AND (a2="+ForumAlgorithm.MODULE_SIGNATURE+" OR a2="+ForumAlgorithm.THEME_SIGNATURE+" OR a2="+ForumAlgorithm.ALIAS_SIGNATURE+")");
            stmt.close();
            genericDouble(pivotsConn, tableName, "src_id", "dest_id", Operation.SIMPLE_TEMP_TABLE);
            closeConnection(pivotsConn); // close the connection. and we can start a new connection delete obsolete data
            purgeObsolete();
            logger.info("End double pivot processing");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void purgeObsolete() throws SQLException {
        Connection pivotsConn = getPivotsConnection();
        Statement stmt = pivotsConn.createStatement();
        stmt.executeUpdate("DELETE FROM pivots_match WHERE pivot_id="+PID+" AND a4<>"+ timestamp);
        stmt.close();
        closeConnection (pivotsConn);
    }

    public void cleanup() {

    }

    protected enum Operation {SIMPLE, SIMPLE_TEMP_TABLE};

    protected void genericDouble(Connection conn, String tableName, String itemField, String viaField, Operation op){
        switch (op) {
            case SIMPLE:
            case SIMPLE_TEMP_TABLE:
                // if the table is temporary, we have to create a new temp table because of MySQL limitation
                // that temporary table can not be reopen in the same query.
                // but it's still better than creating a physical table.
                try {
                    String shadowTableName = tableName;
                    if (op == Operation.SIMPLE_TEMP_TABLE) {
                        shadowTableName = tableName + "_shadow";
                        Statement stmt = conn.createStatement();
                        stmt.executeUpdate("CREATE TEMPORARY TABLE IF NOT EXISTS "+shadowTableName+" LIKE "+tableName);
                        stmt.close();
                        stmt = conn.createStatement();
                        stmt.executeUpdate("INSERT INTO "+shadowTableName+" SELECT * FROM "+tableName);
                        stmt.close();
                    }
                    Statement stmt = conn.createStatement();
                    stmt.executeUpdate("INSERT INTO pivots_match("+itemField+", "+viaField+", pivot_id, score, a4) "+
                            "SELECT a."+itemField+", b."+itemField+", "+PID+", count(a."+viaField+"), "+ timestamp +" FROM "
                            +tableName+" a INNER JOIN "+shadowTableName+" b ON a."+viaField+"=b."+viaField
                            +" GROUP BY a."+itemField+", b."+itemField);
                    stmt.close();
                    break;
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
        }
    }
}
