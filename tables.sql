-- MySQL dump 10.13  Distrib 5.1.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pivots
-- ------------------------------------------------------
-- Server version	5.1.54-1ubuntu4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pivots_mab_brezzilai`
--

DROP TABLE IF EXISTS `pivots_mab_brezzilai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pivots_mab_brezzilai` (
  `src` int(10) unsigned NOT NULL,
  `dst` int(10) unsigned NOT NULL,
  `gittins` double NOT NULL,
  `a` double NOT NULL,
  `b` double NOT NULL,
  `new_click` int(10) unsigned NOT NULL DEFAULT '0',
  `new_pageview` int(10) unsigned NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`src`,`dst`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pivots_mab_brezzilai`
--

LOCK TABLES `pivots_mab_brezzilai` WRITE;
/*!40000 ALTER TABLE `pivots_mab_brezzilai` DISABLE KEYS */;
/*!40000 ALTER TABLE `pivots_mab_brezzilai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pivots_mab_ga`
--

DROP TABLE IF EXISTS `pivots_mab_ga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pivots_mab_ga` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(20) NOT NULL,
  `action` varchar(50) NOT NULL,
  `uniqcount` bigint(15) unsigned NOT NULL,
  `start` date NOT NULL,
  `end` date DEFAULT NULL,
  `status` tinyint(2) unsigned zerofill NOT NULL DEFAULT '00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pivots_mab_ga`
--

LOCK TABLES `pivots_mab_ga` WRITE;
/*!40000 ALTER TABLE `pivots_mab_ga` DISABLE KEYS */;
/*!40000 ALTER TABLE `pivots_mab_ga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pivots_match`
--

DROP TABLE IF EXISTS `pivots_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pivots_match` (
  `src_id` int(11) unsigned NOT NULL DEFAULT '0',
  `dest_id` int(11) unsigned NOT NULL DEFAULT '0',
  `pivot_id` int(11) unsigned NOT NULL DEFAULT '0',
  `score` double NOT NULL DEFAULT '0',
  `s1` double DEFAULT NULL,
  `s2` double DEFAULT NULL,
  `s3` double DEFAULT NULL,
  `s4` double DEFAULT NULL,
  `a1` int(12) DEFAULT NULL,
  `a2` int(12) DEFAULT NULL,
  `a3` int(12) DEFAULT NULL,
  `a4` int(12) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `src_id` (`src_id`,`dest_id`,`pivot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pivots_match`
--

LOCK TABLES `pivots_match` WRITE;
/*!40000 ALTER TABLE `pivots_match` DISABLE KEYS */;
/*!40000 ALTER TABLE `pivots_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pivots_mab_brezzilai_snapshot`
--

DROP TABLE IF EXISTS `pivots_mab_brezzilai_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pivots_mab_brezzilai_snapshot` (
  `src` int(10) unsigned NOT NULL,
  `dst` int(10) unsigned NOT NULL,
  `gittins` double NOT NULL,
  `a` double NOT NULL,
  `b` double NOT NULL,
  `last_click` int(10) unsigned NOT NULL DEFAULT '0',
  `last_pageview` int(10) unsigned NOT NULL DEFAULT '0',
  `snapshot` int(10) unsigned NOT NULL,
  PRIMARY KEY (`src`,`dst`,`snapshot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pivots_mab_brezzilai_snapshot`
--

LOCK TABLES `pivots_mab_brezzilai_snapshot` WRITE;
/*!40000 ALTER TABLE `pivots_mab_brezzilai_snapshot` DISABLE KEYS */;
/*!40000 ALTER TABLE `pivots_mab_brezzilai_snapshot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pivots_mab_display_snapshot`
--

DROP TABLE IF EXISTS `pivots_mab_display_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pivots_mab_display_snapshot` (
  `src_id` int(11) unsigned NOT NULL DEFAULT '0',
  `dest_id` int(11) unsigned NOT NULL DEFAULT '0',
  `score` double NOT NULL DEFAULT '0',
  `snapshot` int(10) unsigned NOT NULL,
  PRIMARY KEY (`src_id`,`dest_id`,`snapshot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pivots_mab_display_snapshot`
--

LOCK TABLES `pivots_mab_display_snapshot` WRITE;
/*!40000 ALTER TABLE `pivots_mab_display_snapshot` DISABLE KEYS */;
/*!40000 ALTER TABLE `pivots_mab_display_snapshot` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-05-12 16:44:27
