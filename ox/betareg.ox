/*********************************************************************
 * 
 * PROGRAM: betareg_example.ox 
 *
 * USE: Estimates the parameters of a beta regression model for the 
 *      Prater gasoline data. 
 *
 * AUTHORS: Francisco Cribari & Silvia Ferrari. 
 *
 * DATE: October 14, 2002. 
 *
 * VERSION: 0.80. 
 *
 * LAST MODIFIED: December 8, 2003.
 *
 *********************************************************************/
 
 /* header files */  
 #include <oxstd.h>
 #include <oxprob.h>   
 #import <maximize> 
 #include<oxdraw.h> 
 
 /* global variables */ 
 static decl s_vy; 
 static decl s_mX; 
 static decl s_mXr; 

 /* select the regression to be estimated: 1 for naive complete, 
    2 for the data with a wrong entry, 3 for the grouped data 
    set */ 

 const decl CASE = 8; 
 
 /* log-likelihood function */ 
 floglik(const vP, const adFunc, const avScore, const amHess)
 {	
   decl k = rows(vP) - 1; 
   decl eta = s_mX*vP[0:(k-1)]; 
   decl mu = exp(eta) ./ (1.0+exp(eta)); 
   decl phi = vP[k]; 
   decl ynew = log( s_vy ./ (1.0-s_vy) );
   decl munew = polygamma(mu*phi, 0) - polygamma((1.0-mu)*phi, 0);
   decl T = diag( exp(eta) ./ (1.0+exp(eta)) .^2 );

   adFunc[0] = double( sumc( loggamma(phi) - loggamma(mu*phi)
               - loggamma((1-mu)*phi) + (mu*phi-1) .* log(s_vy)
	       + ( (1-mu)*phi-1 ) .* log(1-s_vy) ));

   if(avScore)
   {  
      (avScore[0])[0:(k-1)] = phi*s_mX'*T*(ynew-munew); 
      (avScore[0])[k] = double(sumc( polygamma(phi, 0) - mu .* 
         polygamma(mu*phi, 0) - (1.0-mu) .* polygamma( (1.0-mu)*phi, 0) +
         mu .* log(s_vy) + (1.0-mu) .* log(1.0-s_vy) )); 
   }	     	  
 
   if( isnan(adFunc[0]) || isdotinf(adFunc[0]) ) 
 	return 0; 
 
   else
        return 1; // 1 indicates success	
 }

 /* log-likelihood function */ 
 floglikr(const vP, const adFunc, const avScore, const amHess)
 {	
   decl eta = s_mXr*vP[0:0]; 
   decl mu = exp(eta) ./ (1.0+exp(eta)); 
   decl phi = vP[1]; 
   decl ynew = log( s_vy ./ (1.0-s_vy) );
   decl munew = polygamma(mu*phi, 0) - polygamma((1.0-mu)*phi, 0);
   decl T = diag( exp(eta) ./ (1.0+exp(eta)) .^2 );

   adFunc[0] = double( sumc( loggamma(phi) - loggamma(mu*phi)
               - loggamma((1-mu)*phi) + (mu*phi-1) .* log(s_vy)
	       + ( (1-mu)*phi-1 ) .* log(1-s_vy) ));

   if(avScore)
   {  
      (avScore[0])[0] = phi*s_mXr'*T*(ynew-munew); 
      (avScore[0])[1] = double(sumc( polygamma(phi, 0) - mu .* 
               polygamma(mu*phi, 0) - (1.0-mu) .* polygamma( (1.0-mu)*phi, 0) +
               mu .* log(s_vy) + (1.0-mu) .* log(1.0-s_vy) )); 
   }	     	  
 
   if( isnan(adFunc[0]) || isdotinf(adFunc[0]) ) 
 	return 0; 
 
   else
        return 1; // 1 indicates success	
 }
 
 
 main()
 {
 
      decl mu, phi, data, k, ir1, vp1, dfunc1, ybar, yvar, 
           betaols, ynew, dExecTime, nobs, olserrorvar, olsfitted,
           olsfittednew, p; 
      decl psi1, psi2, muhat, phihat, etahat, W, T, vc, D, g, fisherinv;
      decl pseudoR2, dfuncr, ir2, vp2;   
      decl residualstd, H; 
      decl Menvelope, fail, ir3, j, betaobsenv, dfunc3, vp3, ygen; 
      decl psi1gen, psi2gen, Wgen, Hgen, tempinvgen, muhatgen, phihatgen, etahatgen, i,
           residualstdabs, xqq, yqq, cook, tempinv, K1, K2, stderrors, zstats, h, 
           ynewbar, muhatr, residualdev, GL; 
      decl Menvelope2, yqq2, residualdevabs, mysign, ell1, ell2, GL1, GL2;
      decl M, f, e, XQXinv, Q, ystar, mustar; 

      // start clock to time the execution of the program 
      dExecTime = timer();
     
      // read the data
      if(CASE == 0)
      {
          data = loadmat("doctr.mat");
          k = columns(data); 
          s_mX = 1~data[][0:(k-2)];
          s_vy = data[][k-1]/100;
      }
      else if(CASE == 1)
      {
          data = loadmat("ctr_new.mat");
          k = columns(data); 
          s_mX = 1~data[][0:(k-2)];
          s_vy = data[][k-1]/100;
      }
      else if(CASE == 2)
      {
          data = loadmat("ctr_new.mat");
          k = columns(data); 
          s_mX = 1~data[][0:1]~data[][6:7];
          s_vy = data[][k-1]/100;
      }
      else if(CASE == 3)
      {
          data = loadmat("ctr_new.mat");
          k = columns(data); 
          // data = data[0:1][]|data[4:][];
          s_mX = 1~data[][0:1]~data[][6:(k-2)];
          s_vy = data[][k-1]/100;
      }
      else if(CASE == 5)
      {
          data = loadmat("ctr_ox_nooutliner.mat");
          k = columns(data);
          s_mX = 1~data[][0:(k-2)];
          s_vy = data[][k-1]/100;
      }
      else if(CASE == 6)
      {
          data = loadmat("ctr_ox_nooutliner.mat");
          k = columns(data);
          s_mX = 1~data[][0:(k-2)];
          s_vy = data[][k-1]/10;
      }
      else if(CASE == 7)
      {
          data = loadmat("latest.mat");
          k = columns(data);
          s_mX = 1~data[][0:(k-2)];
          s_vy = data[][k-1]/100;
      }
      else if(CASE == 8)
      {
          data = loadmat("dummy.matrix");
          k = columns(data);
          s_mX = 1~data[][0:(k-3)];
          s_vy = data[][k-2]/100;
      }
      else
      {
          println("\n\nERROR: DATASET NOT FOUND!\n\n"); 
          exit(1); 
      }

      nobs = rows(data); 
      p = columns(s_mX);

      if(max(s_vy) >= 1.0 || min(s_vy) <= 0.0)
      {
          println("\n\nERROR: DATA OUT OF RANGE (0,1)!\n\n"); 
          println(max(s_vy));
          println(min(s_vy));
          exit(2); 
      }

      if(p >= nobs)
      {
           println("\n\nERROR: NUMBER OF COVARIATES CANNOT EXCEED NUMBER OF OBSERVATIONS!\n\n"); 
           exit(3); 
      }
      
      ynew = log( s_vy ./ (1.0-s_vy) );

      if(p > 1)
      {
           ols2c(ynew, s_mX, &betaols); 
      } 
		
      else if(p==1)
      {
           betaols = meanc(ynew);
      }

      olsfittednew = s_mX*betaols; 
      olserrorvar = sumsqrc(ynew-olsfittednew)/(nobs-p); 
      olsfitted = exp(olsfittednew) ./ (1 + exp(olsfittednew)); 

      ybar = meanc(s_vy); 
      yvar = varc(s_vy);   

      // starting values
      vp1 = betaols|(meanc( 1 ./ (olserrorvar*(olsfitted .* 
            (1.0-olsfitted)))) - 1.0);

      println("\nBETA REGRESSION ESTIMATION");
      println("--------------------------"); 
     
      println("\nMEAN AND VARIANCE OF Y: ", "%10.5f", ybar~yvar);  
      println("\nINITIAL VALUES FOR THE ML ESTIMATION: ", "%16.5f", vp1);  
      
      // maximum likelihood estimation 
      ir1 = MaxBFGS(floglik, &vp1, &dfunc1, 0, FALSE);
      
      println("\nCONVERGENCE STATUS: ", MaxConvergenceMsg(ir1));
      // println(MaxConvergenceMsg(ir1)); 

      // if convergence, compute standard errors 
      if(ir1 == MAX_CONV || ir1 == MAX_WEAK_CONV)
      {
           etahat = s_mX*vp1[0:(p-1)]; 
           muhat = exp(etahat) ./ (1+exp(etahat)); 
           phihat = vp1[p]; 
           psi1 = polygamma(muhat*phihat, 1); 
           psi2 = polygamma((1.0-muhat)*phihat, 1); 
           T = diag( exp(etahat) ./ (1.0+exp(etahat)) .^2 );
           W = diag(phihat*(psi1+psi2)) * T .^2; 
           vc = phihat*(psi1.*muhat-psi2.*(1-muhat)); 
           D = diag(psi1.*(muhat.^2)+psi2.*(1.0-muhat).^2-polygamma(phihat,1));
	   tempinv = invertsym(s_mX'*W*s_mX); 
           g = trace(D)-(1.0/phihat)*vc'*T'*s_mX*tempinv*s_mX'*T*vc; 
           K1 = tempinv*(g*unit(p)+(1/phihat)*s_mX'*T*vc*vc'*T'*s_mX*tempinv);
           K2 = -tempinv*s_mX'*T*vc; 
           fisherinv = (1/(phihat*g))*((K1~K2)|(-vc'*T'*s_mX*tempinv~phihat));
           println("\nPARAMETER ESTIMATES AND ASYMPTOTIC STANDARD ERRORS: ");
           stderrors = sqrt(diagonal(fisherinv))'; 
           zstats = vp1 ./ stderrors; 
           println("%16.5f", "%c", {"estimates", "std. errors", "z stats", "p-values"}, 
                vp1~stderrors~zstats~2.0*(1.0-probn(fabs(zstats))));
           println("\nASYMPTOTIC COVARIANCE MATRIX OF ML ESTIMATES:"); 
           println("%14.5f", fisherinv); 
           H = sqrt(W)*s_mX*tempinv*s_mX'*sqrt(W);
           h = diagonal(H); 
           ystar = ynew; 
           mustar = polygamma(muhat*phihat, 0) - polygamma((1.0-muhat)*phihat, 0);
           Q = diag( (phihat*(polygamma(muhat*phihat, 1) + polygamma((1-muhat)*phihat, 1)) 
	     - (ystar-mustar).*(1-2*muhat) ./ (muhat.*(1-muhat)) ).*(muhat.^2).*(1-muhat).^2  ); 
	   f = vc - (ystar-mustar); 
           e = -(s_vy-muhat) ./ (s_vy .* (1-s_vy));  
           XQXinv = invertsym(s_mX'*Q*s_mX); 
           M = diag( 1 ./ (s_vy .* (1.0-s_vy)) );  
	   GL1 = T*s_mX*XQXinv*s_mX'*T*M; 
           GL2 = (1.0/(g*phihat))*T*s_mX*XQXinv*s_mX'*T*f*(f'*T*s_mX*XQXinv*s_mX'*T*M-e'); 
           GL = GL1 + GL2; 
           println("diagonal(GL): ", diagonal(GL));
           println("diagonal(GL1): ", diagonal(GL1));
           println("diagonal(GL2): ", diagonal(GL2));
           println("diagonal(H): ", diagonal(H));
           println("\nSUM OF DIAGONAL H ELEMENTS: ", int(sumr(h)) );
           println("\nMIN AND MAX DIAGONAL H ELEMENTS: ", "%8.4f", double(min(h))~double(max(h)) );
           println("\nTHRESHOLD 2P/N: ", "%8.4f", 2.0*p/nobs); 
           println("\nNUMBER OF LEVERAGED OBS.: ", int(sumr(h .> 2.0*p/nobs))); 
           // pseudo-R2
           s_mXr = ones(nobs, 1);
           ynewbar = meanc(ynew); 
           muhatr = exp(ynewbar)/(1.0 + exp(ynewbar)); 
           vp2 = ynewbar|(1.0/(varc(ynew)*muhatr*(1.0-muhatr))-1.0);  
           // restricted maximum likelihood estimation 
           ir2 = MaxBFGS(floglikr, &vp2, &dfuncr, 0, FALSE);

           if(ir2 == MAX_CONV || MAX_WEAK_CONV)
           {
                 pseudoR2 = (correlation(etahat~ynew)[0][1])^2;
                 println("\nPSEUDO R2: ", "%8.4f", double(pseudoR2)); 
           } 


           ell1 = loggamma(phihat) - loggamma(s_vy*phihat) 
	             - loggamma((1-s_vy)*phihat) + (s_vy*phihat-1) .* log(s_vy) 
		     + ( (1-s_vy)*phihat-1 ) .* log(1-s_vy) ;

           ell2 = loggamma(phihat) - loggamma(muhat*phihat) 
	             - loggamma((1-muhat)*phihat) + (muhat*phihat-1) .* log(s_vy) 
		     + ( (1-muhat)*phihat-1 ) .* log(1-s_vy) ;
           residualstd = sqrt(1.0+phihat)*(s_vy-muhat)./sqrt((1.0-h').*muhat.*(1-muhat)); 
	   mysign = (s_vy - muhat) .> 0 .? 1.0 .: -1.0; 
           residualdev = mysign .* sqrt( 2.0*(ell1-ell2) );  
	   cook = (h' .* residualstd.^2) ./ (p*(1-h')); 

           Menvelope = zeros(nobs, 19); 
	   Menvelope2 = zeros(nobs, 19);
	   
           fail = 0; 

           for(j=0; j<19; j++)
           {
                 ygen = zeros(nobs, 1); 
                 for(i=0; i<nobs; i++)
                 {
                     ygen[i] = ranbeta(1, 1, muhat[i]*phihat, (1-muhat[i])*phihat);  
                 }
                 // envelope maximum likelihood estimation 
                 vp3 = vp1; 
                 ir3 = MaxBFGS(floglik, &vp3, &dfunc3, 0, FALSE);
                 if(ir3 != MAX_CONV && ir3 != MAX_WEAK_CONV)
                 {
                      j--; 
                      fail++;
                      continue; 
                 }

                 psi1gen = polygamma(muhat*phihat, 1); 
                 psi2gen = polygamma((1.0-muhat)*phihat, 1); 
                 Wgen = diag(phihat*(psi1+psi2)) * T .^2; 
                 tempinvgen = invertsym(s_mX'*Wgen*s_mX); 
                 Hgen = sqrt(Wgen)*s_mX*tempinvgen*s_mX'*sqrt(Wgen);
                 etahatgen = s_mX*vp3[0:(p-1)]; 
                 muhatgen = exp(etahatgen)./(1.0+exp(etahatgen)); 
                 phihatgen = vp3[p]; 
                 Menvelope[][j] = sqrt(1.0+phihatgen)*(ygen-muhatgen)./sqrt((1.0
                                - diagonal(Hgen)').*muhatgen.*(1-muhatgen));
           }

           residualstdabs=fabs(residualstd); 
           Menvelope = sortc(fabs(Menvelope)); 
           xqq = quann( (range(1,nobs)'+nobs-1/8)/(2*nobs+0.5) ); 
           yqq = zeros(nobs, 4); 
           yqq[][3] = sortc(residualstdabs); 
           yqq[][0] = minc(Menvelope')'; 
           yqq[][1] = meanr(Menvelope); 
           yqq[][2] = maxc(Menvelope')'; 
	   
	   residualdevabs=fabs(residualstd); 
           Menvelope2 = sortc(fabs(Menvelope2)); 
           yqq2 = zeros(nobs, 4); 
           yqq2[][3] = sortc(residualstdabs); 
           yqq2[][0] = minc(Menvelope2')'; 
           yqq2[][1] = meanr(Menvelope2); 
           yqq2[][2] = maxc(Menvelope2')'; 

           SetDraw(SET_FONT, 200); 

           /* first plot */ 
           DrawMatrix(0, residualstd', 0, 1.0, 1.0, 1);  
           DrawAxis(0, TRUE, -3.0, 0, nobs, 5, 5, 5, 0); 
           DrawText(0, "standardized residuals v. indices of obs.", 0, 0, -1, 250, TRUE); 
           DrawText(0, "4", 4, -2.0, -1, 200); 
           /* end of first plot */

           /* second plot */ 
           DrawMatrix(1, residualdev', 0, 1.0, 1.0, 1);  
           DrawAxis(1, TRUE, -3.0, 0, nobs, 5, 5, 5, 0); 
           DrawText(1, "deviance residuals v. indices of obs.", 0, 0, -1, 250, TRUE); 
           DrawText(1, "4", 4, -2.0, -1, 200); 
           /* end of second plot */

           /* third plot */ 
           DrawXMatrix(2, yqq2', 0, xqq', 0, 0); 
           DrawAdjust(ADJ_SYMBOLUSE, 1); 
           DrawAdjust(ADJ_SYMBOL, PL_PLUS);
           DrawText(2, "half-normal plot of deviance residuals", 0, 0, -1, 250, TRUE); 
           DrawText(2, "4", 2.3, 1.9, -1, 200); 
           /* end of third plot */ 

           /* fourth plot */
           DrawXMatrix(3, residualstd', 0, etahat', 0, 0); 
           DrawAdjust(ADJ_SYMBOLUSE, 1); 
           DrawText(3, "standardized residuals v. linear predictor", 0, 0, -1, 250, TRUE); 
           DrawText(3, "4", 0, -1.9, -1, 200); 
           /* end of fourth plot */ 

           /* fifth plot */ 
           DrawMatrix(4, cook', 0, 1.0, 1.0, 1);
           DrawText(4, "Cook's distances v. indices of obs.", 0, 0, -1, 250, TRUE); 
           DrawText(4, "4", 4.5, 0.16, -1, 200); 
           /* end of fifth plot */ 

           /* sixth plot */ 
           DrawXMatrix(5, diagonal(GL), 0, muhat', 0, 0); 
           DrawAdjust(ADJ_SYMBOLUSE, 1); 
           DrawText(5, "generalized leverage v. predicted values", 0, 0, -1, 250, TRUE); 
           DrawText(5, "4", 0.49, 0.45, -1, 200); 
           DrawText(5, "29", 0.24, 0.61, -1, 200); 
           /* end of sixth plot */ 

	   
           SaveDrawWindow("prater_residualplot.eps"); 
      } 

       
      // print date, time, and execution time
      print( "\nDATE: ", date() );
      print( "\nTIME: ", time(), "\n" );
      print( "\nTOTAL EXECUTION TIME: ", timespan(dExecTime) );
      print( "\n" );
 }
 
