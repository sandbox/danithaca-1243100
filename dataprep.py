import MySQLdb, os
from collections import defaultdict

def db_connection():
  path = os.getcwd() + r"/mysql.conf"
  return MySQLdb.connect(read_default_file = path)

def db_conn():
  conn = db_connection()
  cursor = conn.cursor()
  return conn, cursor

# rank items from each of 5001-5006 algorithms.
def rank_pivots_match():
  conn = db_connection()
  cursor = conn.cursor()
  cursor.execute("SELECT DISTINCT pivot_id, src_id FROM pivots_match")
  rows = cursor.fetchall()

  for (pivot_id, src_id) in rows:
    rank = 1
    cursor.execute("SELECT dest_id FROM pivots_match WHERE pivot_id=%s AND src_id=%s ORDER BY score DESC", (pivot_id, src_id))
    ids = cursor.fetchall()
    for (dest_id,) in ids:
      cursor.execute("UPDATE pivots_match SET a3=%s WHERE pivot_id=%s AND src_id=%s AND dest_id=%s", (rank, pivot_id, src_id, dest_id))
      rank += 1
  conn.close()



# purge those results that don't have D5/D6/D7 releases
def purge_no_release():
  valid_proj = set([])
  conn = db_connection()
  cursor = conn.cursor()
  cursor.execute("SELECT p.nid, p.uri, c.directory FROM project_projects p INNER JOIN cvs_projects c \
                  ON p.nid=c.nid INNER JOIN node n ON p.nid=n.nid WHERE c.rid=2 AND n.status=1 AND p.nid<>3060")
  rows = cursor.fetchall()
  print "Total projects:", len(rows)

  for row in rows:
    nid, uri, directory = row
    #print "Processing", uri
    cursor.execute("SELECT title FROM node WHERE type='project_release' AND title LIKE '"+uri+"%'")
    tt = cursor.fetchall()
    for t in tt:
      pl = t[0].split()
      if len(pl)>1:
        r = pl[1][0]
        if r in ('5', '6', '7'): valid_proj.add(int(nid))

  print "Total valid projects:", len(valid_proj)
  cursor.execute("DELETE FROM pivots_match WHERE dest_id NOT IN " + str(tuple(valid_proj)).strip("'"))
  conn.close()



# output for stata use
# select top5 from 5001, 5002, 5004, 5005, 5007,
def output_stata_top5():
  header = ['src', 'dst', 's5001', 's5002', 's5004', 's5005', 's5007', 'r5001', 'r5002', 'r5004', 'r5005', 'r5007']
  out = open('data.txt', 'w')
  out.write('\t'.join(header)+'\n')
  conn = db_connection()
  cursor = conn.cursor()
  cursor.execute("SELECT DISTINCT src_id, dest_id FROM pivots_match WHERE pivot_id IN (5001, 5002, 5004, 5005, 5007) AND a3<=5")
  rows = cursor.fetchall()
  for (src, dst) in rows:
    rec = {'src':src, 'dst':dst}
    cursor.execute("SELECT pivot_id, score, a3 FROM pivots_match WHERE src_id=%s AND dest_id=%s \
                    AND pivot_id IN (5001, 5002, 5004, 5005, 5007)", (src, dst))
    items = cursor.fetchall()
    for (pid, score, rank) in items:
      rec['s'+str(pid)] = score
      rec['r'+str(pid)] = rank
    row = [str(rec.get(key, '.')) for key in header]
    out.write('\t'.join(row)+'\n')
  conn.close()
  out.close()


# output for stata use
# select top10 from 5004, 5005, 5007,
def output_stata_top8():
  header = ['src', 'dst', 's5004', 's5005', 's5007', 'r5004', 'r5005', 'r5007', 'pageview']
  out = open('top8stata.txt', 'w')
  out.write('\t'.join(header)+'\n')
  conn = db_connection()
  cursor = conn.cursor()
  cursor.execute("SELECT DISTINCT src_id, dest_id FROM pivots_match WHERE pivot_id IN (5004, 5005, 5007) AND a3<=8")
  rows = cursor.fetchall()
  for (src, dst) in rows:
    rec = {'src':src, 'dst':dst}
    cursor.execute("SELECT pivot_id, score, a3, pageview FROM pivots_match m INNER JOIN projects_stata s ON \
                    m.src_id=s.nid WHERE src_id=%s AND dest_id=%s AND src_id!=3060 \
                    AND pivot_id IN (5004, 5005, 5007)", (src, dst))
    items = cursor.fetchall()
    for (pid, score, rank, pageview) in items:
      rec['s'+str(pid)] = score
      rec['r'+str(pid)] = rank
      rec['pageview'] = pageview
    row = [str(rec.get(key, '0')) for key in header]
    out.write('\t'.join(row)+'\n')
  conn.close()
  out.close()


def dump_release_table(outfile):
  out = open(outfile, 'w')
  row1 = ['nid', 'uri', 'project', 'd5', 'd6', 'd7', 'other']
  out.write('\t'.join(row1)+'\n')

  conn = db_connection()
  cursor = conn.cursor()
  cursor.execute("SELECT p.nid, p.uri, c.directory FROM project_projects p INNER JOIN cvs_projects c \
                  ON p.nid=c.nid WHERE c.rid=2")
  rows = cursor.fetchall()

  for row in rows:
    nid, uri, directory = row
    print "Processing", uri

    o = [nid, uri, 0, 0, 0, 0, 0]
    if directory.startswith('/modules'):
      o[2] = 1
    elif directory.startswith('/themes'):
      o[2] = 2
    else:
      o[2] = 3

    cursor.execute("SELECT title FROM node WHERE type='project_release' AND title LIKE '"+uri+"%'")
    tt = cursor.fetchall()
    for t in tt:
      pl = t[0].split()
      if len(pl)>1:
        r = pl[1][0]
        if r == '5':
          o[3] = 1
        elif r == '6':
          o[4] = 1
        elif r == '7':
          o[5] = 1
        else:
          o[6] = 1
    out.write('\t'.join(map(str,o))+'\n')
  out.close()
  conn.close()


# process the GA data from the baseline and store data to table ga_baseline
def process_ga_baseline():
  conn = db_connection()
  cursor = conn.cursor()
  cursor.execute("TRUNCATE TABLE ga_baseline")
  pidlist = [5008, 5009]
  for pid in pidlist:
    # import pageviews
    cursor.execute("SELECT action, uniqcount FROM pivots_mab_ga WHERE status=26 and category='PivotsPageview_"+str(pid)+"'")
    rows = cursor.fetchall()
    items = []
    for action, uniqcount in rows:
      uniqcount = int(uniqcount)
      nodes = action.split('_')
      src = nodes[0]
      del nodes[0]
      for dst in nodes:
        item = (pid, src, dst, uniqcount, 0, 0.0)
        items.append(item)
#    ss = set([])
#    for i in items:
#      s = (i[0], i[1])
#      if s in ss: print i
#      else: ss.add(s)
    # TODO: multiple values due to the unstable sorting in the pivots table.
    cursor.executemany("INSERT INTO ga_baseline(pid,src,dst,pageview,click,ctr) VALUES(%s,%s,%s,%s,%s,%s) \
                        ON DUPLICATE KEY UPDATE pageview=pageview+VALUES(pageview)", items)
    # import clicks
    cursor.execute("SELECT action, uniqcount FROM pivots_mab_ga WHERE status=26 and category='PivotsClick_"+str(pid)+"'")
    rows = cursor.fetchall()
    for action, uniqcount in rows:
      src, dst = action.split('_')
      cursor.execute("UPDATE ga_baseline SET click=%s WHERE pid=%s AND src=%s AND dst=%s", (uniqcount, pid, src, dst))
    # update ctr
    cursor.execute("UPDATE ga_baseline SET ctr=click/pageview")

  cursor.close()
  conn.close()



# print baseline comparison (5004, 5005, 5006) to stata
def output_stata_baseline(outfile):
  out = open(outfile, 'w')
  header = ['src', 'pg5004', 'ctr5004', 'pg5005', 'ctr5005', 'pg5006', 'ctr5006']
  print >>out, '\t'.join(header)

  conn = db_connection()
  cursor = conn.cursor()
  cursor.execute("SELECT pid, src, max(pageview), sum(click) FROM ga_baseline GROUP BY pid, src")
  rows = cursor.fetchall()
  items = defaultdict(dict)
  for pid, src, pageview, click in rows:
    items[src]['pg'+str(pid)] = pageview
    items[src]['ctr'+str(pid)] = float(click) / float(pageview)
  cursor.close()
  conn.close()

  del header[0]
  for k, v in items.items():
    row = [k]
    row = row + [v[h] if h in v else '.' for h in header]
    print >>out, '\t'.join(map(str,row))
  out.close()


# output data for oxl beta regression
def output_ox(outfile):
  out = open(outfile, 'w')
  header = 'corrhr solr10 interact lnsrcpg ctr'
  conn = db_connection()
  cursor = conn.cursor()

  cursor.execute("CREATE TEMPORARY TABLE ga AS SELECT src, dst, SUM(pageview) pg, SUM(click) clk, (SUM(click)/SUM(pageview))*100 ctr FROM ga_baseline GROUP BY src, dst")
  cursor.execute("SELECT m1.score, m2.score, m1.score*m2.score, LN(ga.pg), ctr FROM ga LEFT OUTER JOIN \
                  pivots_match m1 ON ga.src=m1.src_id AND ga.dst=m1.dest_id AND m1.pivot_id=5004 LEFT OUTER JOIN \
                  pivots_match m2 ON ga.src=m2.src_id AND ga.dst=m2.dest_id AND m2.pivot_id=5005")

  rows = cursor.fetchall()
  print >>out, "%s %s // %s" % (len(rows), len(header.split()), header)
  for row in rows:
    row = list(row)
    if row[4]>=100: row[4] = 99.9999
    if row[4]<=0: row[4] = 0.00001
    print >>out, '\t'.join(map(str, [0 if r==None else r for r in row]))

  cursor.close()
  conn.close()
  out.close()


# output data for oxl beta regression, using dummy variables: dummy d1234=1 if the source module is 1234 otherwise 0.
def output_ox_categorical(outfile):
  out = open(outfile, 'w')
  conn = db_connection()
  cursor = conn.cursor()

  #header: dummy*... corrhr solr10 interact ctr
  cursor.execute("SELECT DISTINCT src FROM ga_baseline")
  srclist = [str(srcid) for srcid, in cursor.fetchall()]
  header = srclist + ['corrhr', 'solr10', 'interact', 'ctr', 'srcid']

  cursor.execute("CREATE TEMPORARY TABLE ga AS SELECT src, dst, SUM(pageview) pg, SUM(click) clk, (SUM(click)/SUM(pageview))*100 ctr FROM ga_baseline GROUP BY src, dst")
  # the reason to use left outer join is that for some pairs we don't have both corrhr and solr10 scores
  cursor.execute("SELECT m1.score, m2.score, m1.score*m2.score, ctr, ga.src FROM ga LEFT OUTER JOIN \
                  pivots_match m1 ON ga.src=m1.src_id AND ga.dst=m1.dest_id AND m1.pivot_id=5004 LEFT OUTER JOIN \
                  pivots_match m2 ON ga.src=m2.src_id AND ga.dst=m2.dest_id AND m2.pivot_id=5005")

  rows = cursor.fetchall()
  print >>out, "%s %s // header: %s" % (len(rows), len(header), '\t'.join(header))
  for corrhr, solr10, interact, ctr, srcid in rows:
    #ctr=float(ctr); corrhr=str(corrhr); solr10=str(solr10); interact=str(interact); srcid=str(srcid)
    if ctr>=100: ctr = 99.99990
    elif ctr<=0: ctr = 0.00001
    srcid = str(srcid)
    row = ['1' if i==srcid else '0' for i in srclist]
    conv = lambda x: str(x) if x!=None else '0'
    row += map(conv, [corrhr, solr10, interact, ctr, srcid])
    print >>out, '\t'.join(row)

  cursor.close()
  conn.close()
  out.close()

# move the newly generated 4153 and mlt_relation data into the pivot_id as desired.
def prep_pivots_data():
  conn, cursor = db_conn()
  # preparing inital data
  if True:
    print 'Preparing inital data'
    cursor.execute("DELETE FROM pivots_match WHERE pivot_id IN (5008, 5009)")
    print "Preparing 5008" # 5008: corrhr
    cursor.execute("INSERT pivots_match(src_id, dest_id, pivot_id, score) SELECT src_id, dest_id, 5008, score FROM pivots_match WHERE pivot_id=4153 AND src_id<>dest_id")
    print "Preparing 5009" # 5009: solr10
    cursor.execute("INSERT pivots_match(src_id, dest_id, pivot_id, score) SELECT src, dst, 5009, score FROM mlt_relation")

  # remove wrong release data. copied from purge_no_release()
  if True:
    print 'Purge no release.'
    valid_proj = set([])
    cursor.execute("SELECT p.nid, p.uri, c.directory FROM project_projects p INNER JOIN cvs_projects c \
                    ON p.nid=c.nid INNER JOIN node n ON p.nid=n.nid WHERE c.rid=2 AND n.status=1 AND p.nid<>3060")
    rows = cursor.fetchall()
    print "Total projects:", len(rows)
    for nid, uri, directory in rows:
      #print "Processing", uri
      cursor.execute("SELECT title FROM node WHERE type='project_release' AND title LIKE '"+uri+"%'")
      tt = cursor.fetchall()
      for t in tt:
        pl = t[0].split()
        if len(pl)>1:
          r = pl[1][0]
          if r in ('6', '7', '8'): valid_proj.add(int(nid))
    print "Total valid projects:", len(valid_proj)
    cursor.execute("DELETE FROM pivots_match WHERE pivot_id IN (5008, 5009) AND dest_id NOT IN " + str(tuple(valid_proj)).strip("'"))

  # keep the top 10 items only. copied from rank_pivots_match
  if True:
    print "Purging results other than the top 10"
    cursor.execute("SELECT DISTINCT pivot_id, src_id FROM pivots_match WHERE pivot_id IN (5008, 5009)")
    rows = cursor.fetchall()
    for (pivot_id, src_id) in rows:
      rank = 1
      cursor.execute("SELECT dest_id FROM pivots_match WHERE pivot_id=%s AND src_id=%s ORDER BY score DESC", (pivot_id, src_id))
      ids = cursor.fetchall()
      for (dest_id,) in ids:
        cursor.execute("UPDATE pivots_match SET a3=%s WHERE pivot_id=%s AND src_id=%s AND dest_id=%s", (rank, pivot_id, src_id, dest_id))
        rank += 1
    cursor.execute("DELETE FROM pivots_match WHERE pivot_id IN (5008, 5009) AND a3>10")

  # append the missing top5 to corr-hr.
  if True:
    print "Creating corr-hr + solr10"
    p5008set = set([])
    cursor.execute("SELECT src_id, count(dest_id) FROM pivots_match WHERE pivot_id=5008 GROUP BY src_id")
    rows = cursor.fetchall()
    for src_id, c in rows:
      p5008set.add(src_id)
      c = int(c)
      assert c<=10
      if c<10:
        # this only append missing recommendations
        cursor.execute("INSERT INTO pivots_match(src_id, dest_id, pivot_id, score) \
          SELECT src_id, dest_id, 5008, score FROM pivots_match WHERE pivot_id=5009 AND src_id=%s AND a3<=%s", (src_id, 10-c))
    # handle projects not in 5008 but in 5009.
    cursor.execute("SELECT DISTINCT src_id FROM pivots_match WHERE pivot_id=5009")
    for src_id, in cursor.fetchall():
      if src_id not in p5008set:
        cursor.execute("INSERT INTO pivots_match(src_id, dest_id, pivot_id, score) \
          SELECT src_id, dest_id, 5008, score FROM pivots_match WHERE pivot_id=5009 AND src_id=%s AND a3<=10", (src_id))

  if False:
    print "Sanity check"
    cursor.execute("SELECT pivot_id, src_id, count(dest_id) FROM pivots_match WHERE pivot_id IN (5008, 5009) GROUP BY pivot_id, src_id HAVING count(dest_id)<>5")
    for pivot_id, src_id, c in cursor.fetchall():
      print pivot_id, src_id, c
    # select pivot_id, count(*) from pivots_match group by pivot_id

  if True:
    print "Preparing dump to scratchvm."
    cursor.execute("CREATE TABLE pivots_dump SELECT pivot_id, src_id, dest_id, score FROM pivots_match WHERE pivot_id IN (5008, 5009)")
    print "Dump prepared in pivots_dump. Please do the following steps:"
    print """1. execute mysqldump pivots_dump on local server, eg.  mysqldump --user=root --password=xxx pivots_staging pivots_dump > dump.sql
2. upload dump to scratchvm
3. execute mysql < dump.sql on scratchvm
4. execute 'insert into pivots_match(src_id, dest_id, pivot_id, score) select src_id, dest_id, pivot_id, score from pivots_dump'
5. drop pivots_dump table at both scratchvm and local server."""





if __name__ == '__main__':
  #dump_release_table('release.txt')
  #rank_pivots_match()
  #purge_no_release()
  #output_stata_top8()
  process_ga_baseline()
  #output_stata_baseline('/Users/danithaca/Desktop/baseline.txt')
  #output_ox('/Users/danithaca/Desktop/latest.mat')
  #output_ox_categorical('dummy.matrix')
  #prep_pivots_data()
