# build lucene more-like-this

from org.apache.lucene.search import PhraseQuery, IndexSearcher, Searcher, TermQuery
from org.apache.lucene.queryParser import QueryParser
from org.apache.lucene.store import FSDirectory
from org.apache.lucene.index import IndexWriter, IndexReader, Term
from org.apache.lucene.analysis import SimpleAnalyzer
from org.apache.lucene.analysis.standard import StandardAnalyzer
from org.apache.lucene.analysis.shingle import ShingleAnalyzerWrapper
from org.apache.lucene.document import Document
import org.apache.lucene.document.Field as Field
import org.apache.lucene.document.Field.Store as FieldStore
from org.apache.lucene.search.similar import MoreLikeThis
from org.apache.lucene.util import Version
from com.ziclix.python.sql import zxJDBC
from java.io import File
import ConfigParser, os, math

lucene_index_path = 'index_lucene_doprojects'

def get_db_conn():
  config = ConfigParser.ConfigParser()
  config.read(os.getcwd() + r"/mysql.conf")
  driver = "org.gjt.mm.mysql.Driver"
  conn = zxJDBC.connect(config.get('client', 'jython_db'), config.get('client', 'user'), config.get('client', 'password'), driver)
  cursor = conn.cursor()
  return conn, cursor

def build_lucene_index():
  directory = FSDirectory.open(File(lucene_index_path))
  analyzer = StandardAnalyzer(Version.LUCENE_30)
  #analyzer = ShingleAnalyzerWrapper(analyzer, 3)
  indexWriter = IndexWriter(directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED);

  conn, cursor = get_db_conn()
  sql = ("SELECT n.nid, n.title, r.body FROM node n INNER JOIN node_revisions r ON n.nid=r.nid AND n.vid=r.vid WHERE n.type='project_project' AND n.status=1")
  cursor.execute(sql)
  rows = cursor.fetchall()
  conn.close()

  print "Total projects to be indexed:", len(rows)
  count = 0
  for nid, title, body in rows:
    if count % 5000 == 0: print "Processing:", count
    doc = Document()
    field = Field("nid", str(nid), FieldStore.YES, Field.Index.NOT_ANALYZED)
    doc.add(field)
    field = Field("title", title, FieldStore.YES, Field.Index.ANALYZED, Field.TermVector.YES)
    doc.add(field)
    field = Field("body", body, FieldStore.YES, Field.Index.ANALYZED, Field.TermVector.YES)
    doc.add(field)
    #field = Field("text", title+' '+teaser, FieldStore.YES, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS)
    #doc.add(field)
    indexWriter.addDocument(doc)
    count += 1
  indexWriter.optimize();
  indexWriter.close();


def lucene_info():
  directory = FSDirectory.open(File(lucene_index_path))
  indexReader = IndexReader.open(directory)
  print "Number of docs in index:", indexReader.numDocs()
  numDocs = indexReader.numDocs()
  #print "Number of terms:", indexReader.getUniqueTermCount()
  if False:
    # this is the official way to do it.
    for docid in range(indexReader.numDocs()):
      assert not indexReader.isDeleted(docid)
      #doc = indexReader.document(i)
      #print doc.get('storyid')
      tv = indexReader.getTermFreqVector(docid, 'text')
      terms = tv.getTerms()
      freqs = tv.getTermFrequencies()
      doclen = sum(freqs)
      for term, freq in zip(terms, freqs):
        print term, freq
        tf = float(freq) / float(doclen)
        print indexReader.docFreq(Term(term, tv.getField()))
        idf = math.log(float(numDocs) / indexReader.docFreq(Term(term, 'text')))
        print tf, idf, tf*idf
      break

  # get all terms
  if True:
    terms = indexReader.terms()
    while terms.next():
      txt = terms.term().text()
      print txt, indexReader.docFreq(Term(txt))


def search():
  directory = FSDirectory.open(File(lucene_index_path))
  indexReader = IndexReader.open(directory)
  print "Number of docs in index:", indexReader.numDocs()
  #print "Number of terms:", indexReader.getUniqueTermCount()
  terms = indexReader.terms()
  while terms.next():
    print terms.term()

  searcher = IndexSearcher(directory)
  query = TermQuery(Term('title', 'content'))
  hits = searcher.search(query, 10)
  print "Total hits:", hits.totalHits
  for result in hits.scoreDocs:
    doc_id = result.doc
    doc = searcher.doc(doc_id)
    print doc.get('nid'), '\n', doc.get('title'), '\n', doc.get('body')
  indexReader.close()


def mlt_search():
  directory = FSDirectory.open(File(lucene_index_path))
  indexReader = IndexReader.open(directory)
  numDocs = indexReader.numDocs()
  print "Number of docs in index:", numDocs

  analyzer = StandardAnalyzer(Version.LUCENE_30)
  mlt = MoreLikeThis(indexReader)
  mlt.setAnalyzer(analyzer)
  mlt.setFieldNames(['title', 'body'])
  #mlt.setBoost(True)
  #mlt.setBoostFactor(1.1)
  #mlt.setMaxDocFreqPct(40)
  mlt.setMaxQueryTerms(10)  # solr10
  mlt.setMaxWordLen(15)
  mlt.setMinDocFreq(1)
  mlt.setMinTermFreq(1)
  mlt.setMinWordLen(3)
  print "MLT params:", mlt.describeParams()
  conn, cursor = get_db_conn()

  count = 0
  for src_id in range(numDocs):
    if count % 5000 == 0: print "Processing:", count
    count += 1
    src = indexReader.document(src_id)
    #print src.get('storyid'), '\n', src.get('title'), '\n', src.get('teaser')
    query = mlt.like(src_id)
    #print '*' * 50

    searcher = IndexSearcher(directory)
    hits = searcher.search(query, 30)
    #print "Total hits:", hits.totalHits
    for result in hits.scoreDocs:
      dst_id = result.doc
      if dst_id == src_id: continue
      dst = searcher.doc(dst_id)
      cursor.execute("INSERT INTO mlt_relation(src, dst, score) VALUE(?, ?, ?)", (src.get('nid'), dst.get('nid'), result.score))
      #print dst.get('storyid'), result.score, '\n', dst.get('title'), '\n', dst.get('teaser')
    searcher.close()

  indexReader.close()
  conn.close()


if __name__ == "__main__":
  build_lucene_index()
  lucene_info()
  mlt_search()
  #search()
