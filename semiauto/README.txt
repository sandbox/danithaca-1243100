Code design decisions (2011-05-12)

* all displayable data stored in {pivots_match} with pivot_id=6001 (all MAB algorithm starts with 6xxx)
* before updating {pivots_match}, first set the old data to have score=0, s1=old_score. then insert new data. finally remove the old data.this is to prevent "phantom" results.
* the update process only focus on current data. for snapshot purpose, use a separate process that do archival only

