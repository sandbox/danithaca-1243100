import com.google.gdata.client.analytics.AnalyticsService
import com.google.gdata.data.analytics.AccountFeed
import com.google.gdata.data.analytics.AccountEntry
import com.google.gdata.client.analytics.DataQuery
import com.google.gdata.data.analytics.DataFeed
import org.apache.commons.codec.binary.Base64;

baseline()

def main() {
  AnalyticsService myService = new AnalyticsService("Pivots4DO");
  def config = new ConfigSlurper().parse(new File('config.ini').toURL())

  def username = config.ga.username;
  def password = new String(Base64.decodeBase64(config.ga.password));
  if (this.args.length >= 2) {
    def start = this.args[0];
    def end = this.args[1];
  } else {
    println "Using default date: " + new Date().format('yyyy-MM-dd')
    def start = new Date().format('yyyy-MM-dd');
    def end = start;
  }
  myService.setUserCredentials(username, password);

  println "Authenticating with GA"
  // get GA profile
  URL feedUrl = new URL("https://www.google.com/analytics/feeds/accounts/default");
  AccountFeed accountFeed = myService.getFeed(feedUrl, AccountFeed.class);
  AccountEntry doProfile;
  accountFeed.getEntries().each {
    if (it.getTitle().getPlainText().equals('drupal.org')) doProfile = it
  }
  def tableId = doProfile.getTableId().getValue();

  // get database connection
  sql = groovy.sql.Sql.newInstance(config.db.connection, config.db.username, config.db.password, 'com.mysql.jdbc.Driver') ;
  if (0 != sql.firstRow("SELECT count(*) as c FROM pivots_mab_ga WHERE status=0").c) {
  println "GA table still has unprocessed data (status==0). Exit";
  return
  }

  // insert data
  def categories = ['PivotsPageview_6001', 'PivotsClick_6001', 'PivotsSuggest_6001']
  //def categories = ['PivotsPageview_5004', 'PivotsClick_5004', 'PivotsPageview_5005', 'PivotsClick_5005', 'PivotsPageview_5006', 'PivotsClick_5006']
  categories.each { category ->
    insertData(myService, tableId, sql, category, start, end, config.pagesize);
  }
}

def insertData(myService, tableId, sql, category, start, end, pagesize) {
  println "Processing: {$category}";
  URL dataUrl = new URL("https://www.google.com/analytics/feeds/data");
  DataQuery query = new DataQuery(dataUrl);
  query.setIds(tableId);
  query.setStartDate(start);
  if (end==null || end==start) {
    end = null
    query.setEndDate(start);
  } else {
    query.setEndDate(end);
  }
  query.setDimensions("ga:eventAction");
  query.setMetrics("ga:uniqueEvents");
  query.setFilters("ga:eventCategory==${category}");
  query.setSort("-ga:uniqueEvents");
  query.setMaxResults(pagesize);
  startIndex = 1;

  // pagination, until no more new data.
  while (true) {
    query.setStartIndex(startIndex);
    DataFeed data = myService.getFeed(query, DataFeed.class)

    total = data.getTotalResults();
    println "Retrieved next {$pagesize} items starting from {$startIndex}. Total: {$total}"
    data.getEntries().each { entry ->
      action = entry.stringValueOf("ga:eventAction");
      count = entry.longValueOf("ga:uniqueEvents");
      sql.executeInsert("""INSERT INTO pivots_mab_ga(category, action, uniqcount, start, end, status)
                VALUES (${category}, ${action}, ${count}, ${start}, ${end}, 0)""");
    }

    if (startIndex+pagesize-1 > total) {
      println "This category has no more items."
      break;
    } else {
      startIndex += pagesize;
    }
  }

}


// function to get baseline GA data
def baseline() {
  AnalyticsService myService = new AnalyticsService("Pivots4DO");
  def config = new ConfigSlurper().parse(new File('config.ini').toURL())

  def username = config.ga.username;
  def password = new String(Base64.decodeBase64(config.ga.password));
  def start = this.args[0];
  def end = this.args[1];
  myService.setUserCredentials(username, password);

  println "Authenticating with GA"
  // get GA profile
  URL feedUrl = new URL("https://www.google.com/analytics/feeds/accounts/default");
  AccountFeed accountFeed = myService.getFeed(feedUrl, AccountFeed.class);
  AccountEntry doProfile;
  accountFeed.getEntries().each {
    if (it.getTitle().getPlainText().equals('drupal.org')) doProfile = it
  }
  def tableId = doProfile.getTableId().getValue();

  // get database connection
  sql = groovy.sql.Sql.newInstance(config.db.connection, config.db.username, config.db.password, 'com.mysql.jdbc.Driver') ;
  if (0 != sql.firstRow("SELECT count(*) as c FROM pivots_mab_ga WHERE status=0").c) {
  println "GA table still has unprocessed data (status==0). Exit";
  return
  }

  // insert data
  def categories = ['PivotsPageview_5008', 'PivotsPageview_5009',
                    'PivotsClick_5008', 'PivotsClick_5009']
  categories.each { category ->
    insertData(myService, tableId, sql, category, start, end, config.pagesize);
  }
}
