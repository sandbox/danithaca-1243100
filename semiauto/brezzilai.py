# MAB algorithm based on Brezzi&Lai

# status code for {pivots_mab_ga}
# 0: to be processed
# 1: archival
# 20: development purpose
# 25: archived data for baseline calculation
# 26: archived baseline data for 5004,5005,5006 between 2010-5-31 and 2010-7-21
# 91: can't process suggestion uri, run suggestion
# 99: requires human attention

# status code for {pviots_mab_brezzilai}
# 0: to be processed
# 1: archival
# 20: development purpose
# 21: archive the initial data from regression (no ctr*10, a' unchanged)
# 22: initial data from regression ctr*10
# 23: archive the initial data from regression w/o a'=1 adjust

# pivots id:
# 5001: corrhr
# 5002: solr10
# 5003: combination of corrhr and solr10
# 5004-5006: new data 2010-feb
# 5007: co-install
# 6001: MAB with brezzi&lai
# 16001: 6001 archival!



from math import log, pi, pow, sqrt, exp, trunc
from random import random, uniform
import os, MySQLdb, sys, csv, numpy
from scipy.stats import beta
from numpy import array
from copy import deepcopy

SIGMA = 0.999

# solve a, b from mean and var
def beta_unpack(mean, var):
  mean = float(mean)
  var = float(var)
  assert mean<=1 and mean>0 and var>0
  b = 1 / ( 1/(1-mean-var/mean) - 1/(1-mean) )
  a = (mean/(1-mean)) * b
  return a, b

# given a, b, update mean and var
def beta_update(a, b):
  a = float(a)
  b = float(b)
  mean = a / (a + b)
  var = (a*b) / ((a+b)*(a+b)*(a+b+1))
  assert mean<=1 and mean>0 and var>0
  return mean, var


def beta_test(mu, var):
  b = 1 / ( 1/(1-mu-var/mu) - 1/(1-mu) )
  a = (mu/(1-mu)) * b
  print mu
  print var
  print a
  print b
  print a/(a+b)
  print a*b/((a+b)*(a+b)*(a+b+1))



def calc_psi(s):
  s = float(s)
  if (s <= 0.2):
    return sqrt(s / 2)
  elif (s <= 1):
    return 0.49 - 0.11 * pow(s, -0.5)
  elif (s <= 5):
    return 0.63 - 0.26 * pow(s, -0.5)
  elif (s <= 15):
    return 0.77 - 0.58 * pow(s, -0.5)
  else:
    t = log(s, 2)
    t = 2*t - log(t, 2) - log(16*pi)
    return pow(t, 0.5)


def calc_gittins(mean, var):
  global SIGMA
  #beta = 0.999
  beta = SIGMA
  c = -log(beta, 2)
  mean = float(mean)
  var = float(var)
  return mean + sqrt(var) * calc_psi( var / (c * mean*(1-mean)) )


# a class for each dst item.
class Arm:

  @staticmethod
  def create(id, mean, var, a, b):
    a = Arm()
    a.id = id
    a.mean, a.var, a.a, a.b = float(mean), float(var), float(a), float(b)
    a.round = 0
    return a

  @staticmethod
  def create_simulation(id, mean, var, rate):
    a = Arm()
    a.id = id
    a.mean = float(mean)
    a.var = float(var)
    a.rate = float(rate)
    a.a, a.b = beta_unpack(a.mean, a.var)
    a.round = 0
    #while a.a<0 or a.b<0:
    #  a.update(1/a.mean, 1)
    return a

  # the psi() piecewise function on page 93.
  def psi(self, s):
    return calc_psi(s)

  # forumula 16 on page 94
  def calc_gittins(self):
    self.gittins = calc_gittins(self.mean, self.var)
    return self.gittins

  def update(self, pageviews, clicks):
    self.a = self.a + clicks
    self.b = self.b + pageviews - clicks
    self.mean, self.var = beta_update(self.a, self.b)
    self.round += 1

  def set_arbitrary_a(self, newa):
    if newa==None: return
    ratio = self.a / self.b
    self.a, self.b = newa, newa/ratio
    self.mean, self.var = beta_update(self.a, self.b)
    #assert self.mean <= 0.005

  def header_to_string(self):
    return ' '*6 + "%16s %9s %9s, %9s, %9s, %9s, %9s" % ('', 'true%', 'mean%', 'var', 'a', 'b', 'gits%')
  def to_string(self):
    return "%16s %8.7f: %8.7f, %8.7f, %8.7f, %8.7f, %8.7f" % (self.id, self.rate*100, self.mean*100, self.var, self.a, self.b, self.calc_gittins()*100)

def random_clicks(pageviews, rate):
  return sum([1 if random()<rate else 0 for i in range(pageviews)])

# simulation for the brezzi&lai algorithm
# using arbitary mean/var from liner regression
def simulate_v1():
  DISPLAY = 3
  orig_items = [
        Arm.create_simulation('views',          .0014217, .0009561, .0014217),
        Arm.create_simulation('contemplate',    .0018094, .0008902, .0018094),
        #Arm.create_simulation('imagecache',     .0005170, .0008713, .000517),
        Arm.create_simulation('date',           .0009086, .0008380, .0009086),
        Arm.create_simulation('imagefield',     .0031664, .0009177, .0031664),
        Arm.create_simulation('filefield',      .0010339, .0008431, .0010339),
        Arm.create_simulation('whisper',        .0010383, .0008399, .0010383),
        #Arm.create_simulation('musite_info',    .0004543, .0008414, .0004543),
        #Arm.create_simulation('cnr',            .0001136, .0008420, .0001136),
        #Arm.create_simulation('gigya',          .0002271, .0008414, .0002271)
        ]
  orig_items.sort(cmp=lambda x,y: cmp(x.rate, y.rate), reverse=True)
  items = orig_items[:]
  last_display = []
  #for i in items: print i.to_string()
  #pageviews = [10 for i in range(100)]
  for round in range(30):
    print "Round:", round
    print items[0].header_to_string()
    for i in orig_items:
      i.calc_gittins()

    items.sort(cmp=lambda x,y: cmp(x.gittins, y.gittins), reverse=True)
    display = items[:DISPLAY]
    sideline = items[DISPLAY:]
    diffset = set(display) - set(last_display)
    last_display = display

    items.sort(cmp=lambda x,y: cmp(x.mean, y.mean), reverse=True)
    topmean = items[:DISPLAY]
    topset = set(topmean) - set(display)

    for i in orig_items:
      if i in diffset: print '** ',
      else: print ' '*3,
      if i in topset: print '## ',
      else: print ' '*3,
      print i.to_string()

    print "display:", [i.id for i in display], "-- sidelined:", [i.id for i in sideline]
    # simulate clicks
    pageviews = 1000
    for i in display:
      i.clicks = random_clicks(pageviews, i.rate)
      i.update(pageviews, i.clicks)
    print "pageviews:", pageviews, "-- clicks:", [(i.id, i.clicks) for i in display]
    print



# simulation class
class Simulation():
  DISPLAY = 3
  ROUNDS = 30
  PAGEVIEW = 50
  extra_sqlwhere_orig_items = "and status=21"
  extra_sqlfield_id = "uri"
  #src = 48429  # default simulate cck module
  src = 606034

  def print_items_snapshot(self, items):
    print "Snapshot of", len(items), "items"
    print items[0].header_to_string()
    for i in items:
      print ' '*6,
      print i.to_string()

  def get_orig_items(self, srcid):
    orig_items = []
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute("select "+self.extra_sqlfield_id+", m, v, a, b, gittins from pivots_mab_brezzilai b inner join projects_stata s on b.dst=s.nid where src=%s "+self.extra_sqlwhere_orig_items, (srcid))
    rows = cursor.fetchall()
    for row in rows:
      a = Arm()
      a.id, a.mean, a.var, a.a, a.b, a.gittins = row
      a.round = 0
      a.rate = beta.ppf(random(), a.a, a.b)
      orig_items.append(a)
    orig_items.sort(cmp=lambda x,y: cmp(x.rate, y.rate), reverse=True)
    conn.close()
    return orig_items

  def run(self):
    self.orig_items = self.get_orig_items(self.src) # 48429: cck
    self.process_simulation()

  def process_simulation(self):
    orig_items = self.orig_items
    items = orig_items[:]
    last_display = []
    #for i in items: print i.to_string()
    #pageviews = [10 for i in range(100)]
    for round in range(self.ROUNDS):
      print "Round:", round
      print items[0].header_to_string()
      for i in orig_items:
        i.calc_gittins()

      items.sort(cmp=lambda x,y: cmp(x.gittins, y.gittins), reverse=True)
      display = items[:self.DISPLAY]
      sideline = items[self.DISPLAY:]
      diffset = set(display) - set(last_display)
      last_display = display

      items.sort(cmp=lambda x,y: cmp(x.mean, y.mean), reverse=True)
      topmean = items[:self.DISPLAY]
      topset = set(topmean) - set(display)

      for i in orig_items:
        if i in diffset: print '** ',
        else: print ' '*3,
        if i in topset: print '## ',
        else: print ' '*3,
        print i.to_string()

      print "display:", [i.id for i in display], "-- sidelined:", [i.id for i in sideline]
      # simulate clicks
      for i in display:
        #i.clicks = random_clicks(self.PAGEVIEW, i.rate)
        i.clicks = numpy.random.binomial(self.PAGEVIEW, i.rate)
        i.update(self.PAGEVIEW, i.clicks)
      print "pageviews:", self.PAGEVIEW, "-- clicks:", sum([i.clicks for i in display]), [(i.id, i.clicks) for i in display]
      print "CTR%:", sum([i.rate for i in display])*100
      print


# set arbitrary a
class SimulationA(Simulation):
  PROFILE = [None, 1, 2, 5, 10]

  def run(self):
    self.orig_items = self.get_orig_items(48429)
    for i in self.orig_items: i.set_arbitrary_a(self.PROFILE[0])
    self.process_simulation()


# according to rahul's email on 2010-2-26
class SimulationRahul(SimulationA):
  DISPLAY = 5
  ROUNDS = 300
  PAGEVIEW = 50
  J = 1000
  ROLLING_STEP = 10
  OUTFILE = 'rahul.txt'
  ROLLINGFILE = 'rahul_rolling.txt'
  src = 48429

  # clicks[M][N]: M simulation for J, each simulation has ROUNDS rounds.
  def process_stats(self, clicks):
    # calculate daily clicks stats
    results = []
    rolling = []
    for r in range(self.ROUNDS):
      dd = array([clicks[d][r] for d in range(self.J)])
      results.append((dd.mean(), dd.std()))
    rclicks = [rolling_sum(c, self.ROLLING_STEP) for c in clicks]
    for r in range(self.ROUNDS - self.ROLLING_STEP + 1):
      dd = array([rclicks[d][r] for d in range(self.J)])
      rolling.append((dd.mean(), dd.std()))
    return results, rolling

  def run(self):
    f = open(self.OUTFILE, 'w')
    fcache = ['' for i in range(self.ROUNDS)]
    rf = open(self.ROLLINGFILE, 'w')
    rfcache = ['' for i in range(self.ROUNDS-self.ROLLING_STEP+1)]

    self.orig_items = self.get_orig_items(self.src)
    for profile in self.PROFILE:
      clicks_j = []
      total_j = []
      for i in self.orig_items: i.set_arbitrary_a(profile)
      print "Processing profile a':", profile
      self.print_items_snapshot(self.orig_items)

      for j in range(self.J):
        c = self.process_simulation_with_stats()
        clicks_j.append(c)
        total_j.append(sum(c))
      results, rolling = self.process_stats(clicks_j)
      totalarray = array(total_j)
      print "Profile a':", profile, " -- mean:", totalarray.mean(), "std:", totalarray.std(), "size:", len(total_j)

      for i in range(self.ROUNDS):
        fcache[i] += str(results[i][0]) + ',' + str(results[i][1]) + ','
      for i in range(self.ROUNDS-self.ROLLING_STEP+1):
        rfcache[i] += str(rolling[i][0]) + ',' + str(rolling[i][1]) + ','

    for l in fcache: print >>f, l
    f.close()
    for l in rfcache: print >>rf, l
    rf.close()

  # return list[ROUNDS] of total clicks in each round.
  def process_simulation_with_stats(self):
    orig_items = deepcopy(self.orig_items)
    items = orig_items[:]
    last_display = []
    round_clicks = []
    for round in range(self.ROUNDS):
      for i in orig_items:
        i.calc_gittins()

      items.sort(cmp=lambda x,y: cmp(x.gittins, y.gittins), reverse=True)
      display = items[:self.DISPLAY]

      total_clicks = 0
      for i in display:
        i.clicks = numpy.random.binomial(self.PAGEVIEW, i.rate)
        i.update(self.PAGEVIEW, i.clicks)
        total_clicks += i.clicks
      round_clicks.append(total_clicks)
    return round_clicks

# run simulation for the baselines.
# FIXME: this is totally make up for Paul's presentation on 2010-06-11. not usable for final research
class SimulationBaseline(Simulation):
  ROUNDS = 300
  PAGEVIEW = 200
  J = 5
  OUTFILE = '/Users/danithaca/Desktop/simbase-optimal.txt'

  def run(self):
    ctr = 0.4
    out = open(self.OUTFILE, 'w')
    for r in range(self.ROUNDS):
      click, pageview = 0, 0
      for j in range(self.J):
        c = random_clicks(self.PAGEVIEW, ctr)
        click += c
        pageview += self.PAGEVIEW
      print >>out, float(click) / float(pageview)



# for the actual display on d.o.
# simulation throughout the day before we update w/ GA data
class SimulationDisplay(Simulation):
  DISPLAY = 5
  LAST_UPDATE_GAP = 7*24*60*60 # a whole week
  extra_sqlwhere_orig_items = "and status=0"
  extra_sqlfield_id = "dst"

  def __init__(self, src, updated, mab_updated):
    self.src = src
    self.updated = updated
    self.mab_updated = mab_updated

  def run(self):
    self.orig_items = self.get_orig_items(self.src)
    simulate_display = self.process_simulation()
    simulate_display_id = set([i.id for i in simulate_display])
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute("SELECT dst FROM pivots_mab_match WHERE pid=6001")
    rows = cursor.fetchall()
    start_display_id = set([row[0] for row in rows])
    if start_display_id != simulate_display_id:
      self.save_to_match(simulate_display)

  def save_to_match(self, display):
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute("UPDATE pivots_mab_match SET pid=16001 WHERE pid=6001 AND src=%s", self.src)
    values = [(6001, self.src, i.id, self.updated, i.gittins) for i in display]
    cursor.executemany("INSERT pivots_mab_match(pid, src, dst, updated, score) VALUES (%s, %s, %s, %s, %s)", values)
    conn.commit()
    conn.close()

  def simulate_click(self, pageview_src, pageview_past, click_past):
    ratio = (self.updated - self.mab_updated) / float(self.LAST_UPDATE_GAP)
    pageview = pageview_src * ratio
    ctr = 0 if pageview_past==0.0 else click_past / pageview_past
    click = pageview * ctr
    pageview = trunc(pageview) + (1 if random()<(pageview-trunc(pageview)) else 0)
    click = trunc(click) + (1 if random()<(click-trunc(click)) else 0)
    return pageview, click


  def process_simulation(self):
    # compute last update cutoff
    last_update_cutoff = self.mab_updated - self.LAST_UPDATE_GAP - 12*60*60  # 12 hours as error
    # collect pageviews/clicks
    conn = db_connection()
    cursor = conn.cursor()
    # total src pageviews in the past week
    sql = "SELECT SUM(uniqcount) FROM pivots_mab_ga WHERE category LIKE 'PivotsPageview_6001' \
          AND action LIKE '"+str(self.src)+"_%' AND status IN (0,1) \
          AND start>(SELECT MIN(ga_start) FROM pivots_mab_map1 WHERE mab_updated>"+str(last_update_cutoff)+")"
    cursor.execute(sql)
    try: pageview_src = float(cursor.fetchone()[0])
    except: pageview_src = 0
    # pageviews/clicks for the (src,dst) pair for CTR
    cursor.execute("SELECT dst, SUM(pageview), SUM(click) FROM pivots_mab_brezzilai \
                    WHERE src=%s AND status IN (0,1) AND updated>%s GROUP BY dst",
                    (self.src, last_update_cutoff))
    rows = cursor.fetchall()
    conn.close()

    arms_dict = {}
    for (dst, pageview_past, click_past) in rows:
      pageview_past, click_past = float(pageview_past), float(click_past)
      arms_dict[dst] = self.simulate_click(pageview_src, pageview_past, click_past)

    items = self.orig_items[:]
    for i in items:
      pageview, click = arms_dict[i.id]
      i.update(pageview, click)
      i.calc_gittins()
    items.sort(cmp=lambda x,y: cmp(x.gittins, y.gittins), reverse=True)
    return items[:self.DISPLAY]



def rolling_sum(alist, step):
  results = []
  length = len(alist) - step + 1
  for i in range(length):
    results.append( sum(alist[i : (i+step)]) )
  return results



def db_connection():
  path = os.getcwd() + r"/mysql.conf"
  return MySQLdb.connect(read_default_file = path)



def get_unixtime():
  conn = db_connection()
  cursor = conn.cursor()
  cursor.execute("SELECT unix_timestamp()")
  t = cursor.fetchone()[0]
  conn.close()
  return t


# load initial data from regression
def load_regression():
  f = open('brezzilai.raw', 'r')
  line = f.readline()
  print line
  updated = get_unixtime()
  conn = db_connection()
  cursor = conn.cursor()
  for line in f:
    line = line.strip()
    if len(line)==0: continue
    src, dst, m, v,  = line.split('\t')
    a, b = beta_unpack(m, v)

    # ATTN: fix negative a,b by simulating 1 click w/ 1/m pageviews
    while a<0 or b<0:
      a += 1
      b += (1/m)-1
      m, v = beta_update(a, b)

    gittins = calc_gittins(m, v)
    cursor.execute("INSERT INTO pivots_mab_brezzilai(src, dst, updated, gittins, m, v, a, b, click, pageview, status) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                     (src, dst, updated, gittins, m, v, a, b, 0, 0, 0))
  conn.commit()
  conn.close()


class BetaRegression():

  def __init__(self, xx):
    self.beta_header = ['1', 'corrhr', 'solr10', 'interact', 'lnsrcpg']
    # original, using ox_done5.out on recommender.si.umich.edu
    self.beta = [-0.3117, 0.83793, 0.30623, -0.72388, -0.61178]
    self.phi = 327.46801
    # CTR*10
    #self.beta = [2.55002, 1.01663, 0.36216, -1.05273, -0.66855]
    #self.phi = 25.31287

    # new regression after 2010-6-11
    self.beta = []
    self.phi = 0
    self.arm = Arm()

    self.arm.mean = BetaRegression.betareg_mu(xx, self.beta)
    self.arm.var = BetaRegression.betareg_var(self.arm.mean, self.phi)
    self.arm.a = BetaRegression.betareg_a(self.arm.mean, self.phi)
    self.arm.b = BetaRegression.betareg_b(self.arm.mean, self.phi)
    self.arm.calc_gittins()

  @staticmethod
  def betareg_mu(xx, beta):
    p = sum([x*b for (x,b) in zip(xx, beta)])
    return exp(p) / (1+exp(p))

  @staticmethod
  def betareg_var(mu, phi):
    return mu*(1-mu)/(1+phi)

  @staticmethod
  def betareg_a(mu, phi):
    return mu*phi

  @staticmethod
  def betareg_b(mu, phi):
    return (1-mu)*phi



# use the beta regression for init mab
def load_regression_beta():
  conn = db_connection()
  cursor = conn.cursor()
  updated = get_unixtime()

  input = open('top8stata.txt', 'r')
  data = csv.reader(input, delimiter="\t")
  header = data.next()
  print header

  for (src, dst, s5004, s5005, s5007, r5004, r5005, r5007, pageview) in data:
    corrhr = float(s5004)
    solr10 = float(s5005)
    interact = corrhr * solr10
    lnsrcpg = log(float(pageview)) if float(pageview)>0 else 0
    pred = BetaRegression([1, corrhr, solr10, interact, lnsrcpg])
    # set a'=1
    if pred.arm.a<1: pred.arm.set_arbitrary_a(1)
    cursor.execute("INSERT INTO pivots_mab_brezzilai(src, dst, updated, gittins, m, v, a, b, click, pageview, status) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                 (src, dst, updated, pred.arm.gittins, pred.arm.mean, pred.arm.var, pred.arm.a, pred.arm.b, 0, 0, 0))

  input.close()
  conn.commit()
  conn.close()



# batch update MAB status from GA data
def update_ga():
  SUGGESTION_MAG_FACTOR = 1
  updated = get_unixtime()
  conn = db_connection()
  cursor = conn.cursor()

  # add a map entry in {pivots_mab_map1}, which is to store the batch id of GA import
  cursor.execute("SELECT DISTINCT start FROM pivots_mab_ga WHERE status=0")
  rows = cursor.fetchall()
  if len(rows) !=1:
    print "No new data or inconsistent GA data. Please check. Exit."
    return
  ga_start = rows[0][0]
  cursor.execute("INSERT INTO pivots_mab_map1(mab_updated, ga_start) VALUE (%s, %s)", (updated, ga_start))


  cursor.execute("SELECT id, category, action, uniqcount FROM pivots_mab_ga WHERE status=0")
  rows = cursor.fetchall()

  ga_dict = {}
  for (ga_id, category, action, uniqcount) in rows:
    uniqcount = int(uniqcount)

    if category.startswith('PivotsPageview'):
      ids = action.split('_')
      src = ids[0]
      for dst in ids[1:]:
        pageview, click = ga_dict.get((src, dst), (0, 0))
        pageview += uniqcount
        ga_dict[(src, dst)] = (pageview, click)

    elif category.startswith('PivotsClick'):
      src, dst = action.split('_')
      pageview, click = ga_dict.get((src, dst), (0, 0))
      click += uniqcount
      ga_dict[(src, dst)] = (pageview, click)

    #FIXME: not tested
    elif category.startswith('PivotsSuggest'):
      i = action.find('_') # has to satisfy the format
      src = action[:i]
      uri = action[i+1:]
      uri = uri[len('http://drupal.org/project/'):]
      cursor.execute('SELECT nid FROM project_projects WHERE uri=%s', uri)
      try:
        dst = cursor.fetchone()[0]
        suggest_count = uniqcount * SUGGESTION_MAG_FACTOR
        pageview, click = ga_dict.get((src, dst), (0, 0))
        pageview += suggest_count
        click += suggest_count
        ga_dict[(src, dst)] = (pageview, click)
      except:
        # mark the ga record as un-recognizable, do nothing else.
        cursor.execute("UPDATE pivots_mab_ga SET status=99 WHERE id=%s", ga_id)

  # archive the ga data.
  cursor.execute("UPDATE pivots_mab_ga SET status=1 WHERE status=0")

  # now we have ga pageview/click data stored in ga_dict, then we update the table
  for ((src, dst), (pageview, click)) in ga_dict.items():
    cursor.execute("SELECT a, b FROM pivots_mab_brezzilai WHERE src=%s AND dst=%s AND status=0", (src, dst))
    try:
      a, b = cursor.fetchone()
    except:
      # (src,dst) not in the pool yet because of new suggestion, make the initial value as 5th from the original data
      a, b = 0, 0
      cursor.execute("SELECT a, b FROM pivots_mab_brezzilai WHERE src=%s AND status=21 ORDER BY gittins DESC LIMIT 5", src)
      # this is to get whatever we can get if we don't have as many as 5 arms. if no arm at all, a,b just start at 0
      for i in range(5):
        try: a, b = cursor.fetchone()
        except: pass
    a = a + float(click)
    b = b + float(pageview) - float(click)
    m, v = beta_update(a, b)
    gittins = calc_gittins(m, v)
    # archive the old record of (src,dst) by set status=1.
    # if a (src,dst) doesn't have pageviews/clicks, then the data remains the same.
    cursor.execute("UPDATE pivots_mab_brezzilai SET status=1 WHERE src=%s AND dst=%s AND status=0", (src, dst))
    # insert the new record w/ status=0, ie, the current value.
    cursor.execute("INSERT INTO pivots_mab_brezzilai(src, dst, updated, gittins, m, v, a, b, click, pageview, status) \
          VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",  (src, dst, updated, gittins, m, v, a, b, click, pageview, 0))
  print "Updated total of", len(ga_dict), "(src, dst) pairs for CTR."

  # commit db changes and close conn.
  conn.commit()
  conn.close()

# generate items to display in pivots
def generate_displayable():
  #define some constants
  #DISPLAYLIMIT = 5  # this is set in SimulationDisplay

  updated = get_unixtime()
  conn = db_connection()
  cursor = conn.cursor()

  # add a map entry in {pivots_mab_map2}, which is to link between brezzilai update (daily) and displayable items update (every 15 mins)
  cursor.execute("SELECT max(updated) FROM pivots_mab_brezzilai")
  mab_updated = cursor.fetchone()[0]
  cursor.execute("INSERT INTO pivots_mab_map2(display_updated, mab_updated) VALUE (%s, %s)", (updated, mab_updated))

  cursor.execute("SELECT DISTINCT src FROM pivots_mab_brezzilai WHERE status=0")
  srcs = cursor.fetchall()
  for (src,) in srcs:
    sim = SimulationDisplay(src, updated, mab_updated)
    sim.run()

  conn.commit()
  conn.close()

def test_ratio_sample():
  cand = [[1, 0.2], [2, 0.1], [3, 0.4], [4, 0.01]]
  for i in range(50):
    print ratio_sample(cand, 2)

# sample and return cand[key] according to ratio in cand[value]
def ratio_sample(cand, num):
  if len(cand) <= num: return cand
  results = []
  cc = list(cand)
  for i in range(num):
    s = sum([c[1] for c in cc])
    r = uniform(0, s)
    for c in cc:
      r -= c[1]
      if r < 0:
        results.append(c)
        cc.remove(c)
        break
  return results



if __name__ == '__main__':
  s = Simulation()
  s.run()
  #update_ga()
  #generate_displayable()
  #load_regression_beta()
  #optimize_a()
