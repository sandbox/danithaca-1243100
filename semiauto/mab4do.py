# for python 2.6.

__author__ = 'danithaca'


import math, random, copy, MySQLdb, itertools, time, os, datetime, sys
import mypytools, csv
from collections import defaultdict
from pprint import pprint


INIT_A, INIT_B = 3, 3

def db_connection():
  path = os.getcwd() + r"/mysql.conf"
  conn = MySQLdb.connect(read_default_file = path)
  conn.autocommit(False) # NOTE: you should manually commit before close the connection.
  return conn, conn.cursor()
  
def drupal_db_connection():
  path = os.getcwd() + r"/drupal.conf"
  conn = MySQLdb.connect(read_default_file = path)
  conn.autocommit(False) # NOTE: you should manually commit before close the connection.
  return conn, conn.cursor()


def get_unixtime():
  conn, cursor = db_connection()
  cursor.execute("SELECT unix_timestamp()")
  t = cursor.fetchone()[0]
  conn.close()
  return t


# invoke groovy or do it manually
def import_ga_data():
  print 'Please manually load GA data using the Groovy script first.'
  s = raw_input('Do you want to continue [y/n]')
  if s == 'y':
    return True
  else:
    sys.exit(-1)


# create snapshot of the database for simulation study.
def snapshot():
  timestamp = int(time.time())
  print "Taking snapshot at:", timestamp
  conn, cursor = db_connection()
  # snapshot brezzilai data
  cursor.execute("INSERT INTO pivots_mab_brezzilai_snapshot(src, dst, gittins, a, b, last_click, last_pageview, snapshot) \
      SELECT src, dst, gittins, a, b, new_click, new_pageview, %s FROM pivots_mab_brezzilai", timestamp)
  # snapshot displayable data
  cursor.execute("INSERT INTO pivots_mab_display_snapshot(src_id, dest_id, score, snapshot) \
      SELECT src_id, dest_id, score, %s FROM pivots_match WHERE pivot_id=%s", (timestamp, BrezziLai.PIVOT_ID))
  conn.commit()
  conn.close()
  print "Finish snapshot"


# update the GA tables, and setup pivots_mab_brezzilai accordingly
# before using this function, manually call GA update first.
def prepare_ga_for_mab():
  SUGGESTION_MAG_FACTOR = 1
  updated = int(time.time())
  conn, cursor = db_connection()
  pivot_id = '6001'
  
  print 'Build project<->nid map'
  uri_map = {}
  drupal_conn, drupal_cursor = drupal_db_connection()
  drupal_cursor.execute("SELECT nid, uri FROM project_projects")
  rows = drupal_cursor.fetchall()
  for nid, uri in rows:
    uri_map[uri] = int(nid)
  
  print 'Cleanup old click/pageview data'
  cursor.execute("UPDATE pivots_mab_brezzilai SET new_click=0, new_pageview=0")

  print 'Processing GA data.'
  cursor.execute("SELECT id, category, action, uniqcount FROM pivots_mab_ga WHERE status=0 AND category LIKE '%"+pivot_id+"'")
  rows = cursor.fetchall()

  ga_dict = {}
  for (ga_id, category, action, uniqcount) in rows:
    uniqcount = int(uniqcount)

    if category == 'PivotsPageview_' + pivot_id:
      ids = action.split('_')
      src = ids[0]
      for dst in ids[1:]:
        pageview, click = ga_dict.get((src, dst), (0, 0))
        pageview += uniqcount
        ga_dict[(src, dst)] = (pageview, click)

    elif category == 'PivotsClick_' + pivot_id:
      src, dst = action.split('_')
      pageview, click = ga_dict.get((src, dst), (0, 0))
      click += uniqcount
      ga_dict[(src, dst)] = (pageview, click)

    elif category == 'PivotsSuggest_' + pivot_id:
      i = action.find('_') # has to satisfy the format
      src, uri = action[:i], action[i+1:]
      uri = uri[len('http://drupal.org/project/'):]
      try:
        dst = uri_map[uri]
        suggest_count = uniqcount * SUGGESTION_MAG_FACTOR
        pageview, click = ga_dict.get((src, dst), (0, 0))
        pageview += suggest_count
        click += suggest_count
        ga_dict[(src, dst)] = (pageview, click)
      except:
        # mark the ga record as un-recognizable, do nothing else.
        cursor.execute("UPDATE pivots_mab_ga SET status=99 WHERE id=%s", ga_id)
    else:
      assert False, 'GA table can only accept PivotsPageview_mab, PivotsClick_mab, PivotsSuggest_mab data'


  # upate pivots_mab_brezzilai table.
  #pprint(ga_dict)
  print 'Total src_dst pairs affected:', len(ga_dict)
  for (src, dst), (new_pageview, new_click) in ga_dict.items():
    #cursor.execute('UPDATE pivots_mab_brezzilai SET new_pageview=%s, new_click=%s WHERE src=%s AND dst=%s',
    #  (new_pageview, new_click, src, dst))
    cursor.execute('INSERT INTO pivots_mab_brezzilai(src, dst, gittins, a, b, new_click, new_pageview) VALUES(%s, %s, %s, %s, %s, %s, %s)\
        ON DUPLICATE KEY UPDATE new_pageview=%s, new_click=%s', (src, dst, 0, INIT_A, INIT_B, new_click, new_pageview, new_pageview, new_click))


  # archive the ga data.
  cursor.execute("UPDATE pivots_mab_ga SET status=1 WHERE status=0")
  
  conn.commit()
  conn.close()
  print "Finish preparing GA for MAB."


def prepare_ga_for_mab_random():
  print 'Randomly generate click/pageview data'
  conn, cursor = db_connection()
  cursor.execute("UPDATE pivots_mab_brezzilai SET new_click=RAND()*10")
  cursor.execute("UPDATE pivots_mab_brezzilai SET new_pageview=RAND()*1000 + new_click")
  conn.commit()
  conn.close()



# this is the main function that drives the MAB process
def update_mab():
  conn, cursor = db_connection()
  cursor.execute("SELECT DISTINCT src FROM pivots_mab_brezzilai")
  src_list = [s for s, in cursor.fetchall()]
  print "Total source projects to be processed for MAB:", len(src_list)

  # handle old data
  cursor.execute("UPDATE pivots_match SET s1=score, score=0 WHERE pivot_id=%s", BrezziLai.PIVOT_ID)

  for src in src_list:
    mab = BrezziLai4DrupalOrg(src, cursor)
    mab.run_once()
    mab.generate_displayable()

  # finally, remove the old data. note that all old data should be archived already.
  cursor.execute("DELETE FROM pivots_match WHERE pivot_id=%s AND score=0", BrezziLai.PIVOT_ID)
  conn.commit()
  conn.close()
  print 'Finish updating MAB.'




# setup the init values for MAB update. only run once at the beginning.
def initialize_mab(pivot_list = [5008, 5009], limit=10):
  init_a, init_b = INIT_A, INIT_B
  conn, cursor = db_connection()

  # get top-10 items to put to the MAB arms pool
  def get_top_items(pivot_id, src_id):
    cursor.execute("SELECT DISTINCT dest_id FROM pivots_match WHERE pivot_id=%s AND src_id=%s \
        AND dest_id<>src_id ORDER BY score DESC LIMIT %s", (pivot_id, src_id, limit))
    rows = cursor.fetchall()
    items = [i for i, in rows]
    return items

  cursor.execute("SELECT DISTINCT src_id FROM pivots_match WHERE src_id<>dest_id AND pivot_id IN "+str(tuple(pivot_list)))
  src_list = cursor.fetchall()
  print 'Total src module to be initialized:', len(src_list)
  for src_id, in src_list:
    items = []
    for pivot_id in pivot_list:
      items += get_top_items(pivot_id, src_id)
    items = mypytools.dedup(items)
    assert len(items) > 0, 'Internal erros. Items cannot be 0'
    records = [(src_id, dst, 0, init_a, init_b, 0, 0) for dst in items]
    # save very simple status in MAB.
    cursor.executemany("INSERT INTO pivots_mab_brezzilai(src, dst, gittins, a, b, new_click, new_pageview) VALUES(%s, %s, %s, %s, %s, %s, %s)", records)

  conn.commit()
  conn.close()
  print "Finished initializing MAB"


# the Arm class. usually not to subclass. put all computation in the MAB class.
class Arm(object):
  def __init__(self, id):
    self.id = id
  def __cmp__(self, other):
    return cmp(self.gittins, other.gittins)
  def to_string(self) :
    msg = self.id + ':' + str(self.gittins)
    attr = copy.deepcopy(self.__dict__)
    del(attr['id'])
    del(attr['gittins'])
    #msg += '(' + str(attr) + ')'
    return msg


# the base class of all MAB algorithms.
class MAB(object):
  ROUNDS = 60
  DISPLAY_ARMS = 5
  PIVOT_ID = 0

  # create the self.arms variable
  def load_arms(self):
    assert False, 'please override'

  # save the self.arms variable
  def save_arms(self):
    assert False, 'please override'

  def print_arms(self):
    sorted_arms = self.get_top_arms()
    for arm in sorted_arms[:self.DISPLAY_ARMS]:
      print arm.to_string()
    print "== didn't display"
    for arm in sorted_arms[self.DISPLAY_ARMS:]:
      print arm.to_string()


  def update_single_arm(self, arm):
    assert False, 'please override'

  # prepare a single round of simulation
  def prepare_simulation_round(self):
    assert False, 'please override'


  def get_top_arms(self, limit = 0):
    sorted_arms = sorted(self.arms, reverse=True)
    if limit>0:
      return sorted_arms[:limit]
    else:
      return sorted_arms


  def run_once(self):
    self.load_arms()
    assert hasattr(self, 'arms'), 'Internal error. Please load arms first.'
    for arm in self.arms:
      self.update_single_arm(arm)
    self.save_arms()


  def simulate(self):
    self.load_arms()

    real_ctr_rounds = []  # sum of CTR in the display
    display_changes = []  # true if display changes.
    dst_pageviews = defaultdict(int)  # sum of total pageviews for each dst.
    previous_display = []

    assert hasattr(self, 'arms'), 'Internal error. Please load arms first.'
    for r in range(self.ROUNDS):
      print '*'*10, 'Round:', r, '*'*10
      self.print_arms()
      self.prepare_simulation_round()
      current_display = self.get_top_arms(self.DISPLAY_ARMS)

      # only do the simulation on the ones got displayed.
      real_ctr = []
      for arm in current_display:
        dst_pageviews[arm.id] += arm.pageview
        self.update_single_arm(arm)
        real_ctr.append(float(arm.click) / float(arm.pageview))
      assert len(real_ctr) == self.DISPLAY_ARMS
      real_ctr_rounds.append(sum(real_ctr))
      if set(current_display) != set(previous_display):
        display_changes.append(True)
      else:
        display_changes.append(False)
      previous_display = current_display
      print

    # no need to save arms in simulation
    #self.save_arms()
    self.real_ctr_rounds = real_ctr_rounds
    self.dst_pageviews = dst_pageviews
    self.display_changes = display_changes
    print "Finish simulation."
    #return real_ctr_rounds





# the class to do BrezziLai MAB algorithms
class BrezziLai(MAB):
  SIGMA = 0.99999999999999
  PIVOT_ID = 6001

  # solve a, b from mean and var
  # only used when we have prior data.
  @staticmethod
  def beta_unpack(mean, var):
    mean = float(mean)
    var = float(var)
    assert mean<=1 and mean>0 and var>=0
    b = 1 / ( 1/(1-mean-var/mean) - 1/(1-mean) )
    a = (mean/(1-mean)) * b
    return math.round(a), math.round(b)

  # given a, b, update mean and var
  @staticmethod
  def beta_update(a, b):
    a = float(a)
    b = float(b)
    assert a>=0 and b>=0 and a+b>=0, (a, b, a+b)
    mean = a / (a + b)
    var = (a*b) / ((a+b)*(a+b)*(a+b+1))
    assert mean<=1 and mean>0 and var>=0
    return mean, var

  @staticmethod
  def calc_psi(s):
    s = float(s)
    if (s <= 0.2):
      return math.sqrt(s / 2)
    elif (s <= 1):
      return 0.49 - 0.11 * math.pow(s, -0.5)
    elif (s <= 5):
      return 0.63 - 0.26 * math.pow(s, -0.5)
    elif (s <= 15):
      return 0.77 - 0.58 * math.pow(s, -0.5)
    else:
      t = math.log(s, 2)
      t = 2*t - math.log(t, 2) - math.log(16*math.pi)
      return math.pow(t, 0.5)

  @staticmethod
  def calc_gittins(mean, var):
    if mean == 1 and var == 0:
      return 1.0
    else:
      beta = BrezziLai.SIGMA
      c = -math.log(beta, 2)
      psi = BrezziLai.calc_psi( var / (c * mean*(1-mean)) )
      gittins = mean + math.sqrt(var) * psi
      return gittins

  #Override
  # arm needs to be set with pagview, click,
  def update_single_arm(self, arm):
    arm.a = arm.a + arm.click
    arm.b = arm.b + arm.pageview - arm.click
    arm.mean, arm.var = self.beta_update(arm.a, arm.b)
    arm.gittins = self.calc_gittins(arm.mean, arm.var)


class BrezziLaiSimulationV1(BrezziLai):
  def __init__(self, src_id, src_mean_pageview, dst_list):
    self.src_id = src_id
    self.src_mean_pageview = src_mean_pageview
    self.dst_list = dst_list

  def load_arms(self):
    self.arms = []
    for dst_id, ctr in self.dst_list:
      arm = Arm(dst_id)
      arm.ctr = ctr
      arm.a, arm.b = 3,3
      arm.mean, arm.var = self.beta_update(arm.a, arm.b)
      arm.gittins = self.calc_gittins(arm.mean, arm.var)
      self.arms.append(arm)


  def prepare_simulation_round(self):
    pageview = int(random.gauss(self.src_mean_pageview, 0.15 * self.src_mean_pageview))
    for arm in self.arms:
      arm.pageview = pageview
      mean_click = pageview * arm.ctr
      arm.click = int(random.gauss(mean_click, 0.5 * mean_click))


  def simulate(self):
    print "Simulation for src:", self.src_id, "with mean pageview:", self.src_mean_pageview
    super(BrezziLaiSimulationV1, self).simulate()
    #print self.real_ctr_rounds


class BrezziLaiSimulationV2(BrezziLaiSimulationV1):
  '''
  changed random genration method.
  '''
  def __init__(self, src_id, src_mean_pageview, dst_list, display_arms = 5):
    #assert src_mean_pageview == 1, 'Pageview has to be 1 in this simulation'
    super(BrezziLaiSimulationV2, self).__init__(src_id, src_mean_pageview, dst_list)
    self.DISPLAY_ARMS = display_arms
    self.best_ctr_rounds = []
    best_ctr_dst = sorted(dst_list, key=lambda r: r[1], reverse=True)
    self.best_ctr_dst = set([r[0] for r in best_ctr_dst[:self.DISPLAY_ARMS]])
    print 'Best CTR items', self.best_ctr_dst
    self.ideal_ctr = sum([r[1] for r in best_ctr_dst[:self.DISPLAY_ARMS]])

  #Override
  def prepare_simulation_round(self):
    #pageview = int(random.gauss(self.src_mean_pageview, 0.15 * self.src_mean_pageview))
    pageview = self.src_mean_pageview
    best_ctr = 0.0
    for arm in self.arms:
      arm.pageview = pageview
      arm.click = mypytools.flip_coin_n(arm.ctr, pageview)
      if arm.id in self.best_ctr_dst:
        best_ctr += float(arm.click) / float(arm.pageview)
    self.best_ctr_rounds.append(best_ctr)



  #Override
  def simulate(self, f):
    super(BrezziLaiSimulationV2, self).simulate()
    assert len(self.best_ctr_rounds) == len(self.display_changes) == len(self.real_ctr_rounds) == self.ROUNDS
    out = open(f, 'w')
    for i, ctr in enumerate(self.real_ctr_rounds):
      change = 1 if self.display_changes[i] else 0
      print >>out, str(ctr) + ',' + str(self.best_ctr_rounds[i]) + ',' + str(self.ideal_ctr) + ',' + str(change)
    out.close()
    print self.dst_pageviews



class BrezziLai4DrupalOrg(BrezziLai):
  GENERATE_MATCH_LIMIT = 5

  def __init__(self, src, cursor):
    self.src = src
    self.cursor = cursor


  # when executing this. you should assume the old data are handled properly to avoid 'phantom'
  def generate_displayable(self):
    cursor = self.cursor
    cursor.execute("INSERT INTO pivots_match(src_id, dest_id, pivot_id, score) \
      SELECT %s, dst, %s, gittins FROM pivots_mab_brezzilai WHERE src=%s ORDER BY gittins DESC LIMIT %s",
      (self.src, self.PIVOT_ID, self.src, self.GENERATE_MATCH_LIMIT))


  # Override
  def load_arms(self):
    cursor = self.cursor
    # note: new_click, new_pageview should be updated by GA script before executing here
    cursor.execute("SELECT dst, a, b, new_click, new_pageview FROM pivots_mab_brezzilai WHERE src=%s", self.src)
    self.arms = []
    for dst, a, b, click, pageview in cursor.fetchall():
      arm = Arm(dst)
      arm.a, arm.b = a, b
      arm.click, arm.pageview = click, pageview
      self.arms.append(arm)

  # Override
  def save_arms(self):
    cursor = self.cursor
    for arm in self.arms:
      #cursor.execute("UPDATE pivots_mab_brezzilai SET a=%s, b=%s, new_click=0, new_pageview=0 WHERE src=%s AND dst=%s", (arm.a, arm.b, self.src, arm.id))
      # we don't update new_click, new_pageview for archival purpose.
      cursor.execute("UPDATE pivots_mab_brezzilai SET gittins=%s, a=%s, b=%s WHERE src=%s AND dst=%s", (arm.gittins, arm.a, arm.b, self.src, arm.id))



# the steps to execute in system cron
def cron():
  print 'Running cron on:', datetime.datetime.now()
  import_ga_data() # this is groovy external script
  snapshot()
  prepare_ga_for_mab()
  update_mab()



if __name__ == '__main__':
  cron()
  sys.exit(0)
  #m = BrezziLaiSimulationV2('cck', 2000,
  #                        [('unrelated', 0.000001), ('cck_c', 0.008), ('cck_a', 0.01), ('cck_b', 0.009), ('views', 0.005), ('cck_super', 0.5)])
  #m = BrezziLaiSimulationV2('cck', 500,
  #                        [('unrelated', 0.000001), ('cck_c', 0.008), ('cck_a', 0.01), ('cck_b', 0.009), ('views', 0.005), ('cck_super', 0.05)])

  input = open('sim1.csv', 'r')
  dst_list = []
  for line in input:
    line = line.strip()
    if line.startswith('#') or len(line) == 0: continue
    line = line.split(',')
    dst_list.append((line[0], float(line[1])/100))

  m = BrezziLaiSimulationV2('cck', 2000, dst_list)
  m.DISPLAY_ARMS = 5
  m.ROUNDS = 60
  #m.SIGMA=0.99999999999999999
  m.simulate('/tmp/mab.csv')
  
  #initialize_mab()
  #snapshot()
  #prepare_ga_for_mab()
  #update_mab()
  #cron()
